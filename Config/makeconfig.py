import random
from itertools import cycle

numsessions = 3
numparticipants = 30

testing_seq_length = 3

numtasks = testing_seq_length * 10 * 5

infile = open("phrases.txt", 'r')
allphrases = infile.readlines()

for i in range(numparticipants):
    # participant number
    p_num = i+1

    # conditions for each session file
    conditions = [0,1,2]

    # if participant number is even
    if p_num % 2 == 0:
        conditions = [1,0,2]

    for j in range(numsessions):
        testing_seq = cycle([0,1,1])
        auto_acc_list = [0, 25, 50, 75, 100]
        accidx = 0
        random.shuffle(auto_acc_list)
        random.shuffle(allphrases)

        # session number
        s_num = j+1
        outfile = open("configs/p" + str(p_num) + "_" +str(s_num) + ".txt", 'w')

        trial,block,testing,condition = 1,1,next(testing_seq),conditions[j]
        accuracy = auto_acc_list[accidx]

        for t in range(numtasks):
            phrase = allphrases[t].strip()

            outfile.write(str(trial) + "," + str(block) + "," + str(testing) + "," + str(condition) + "," + str(accuracy) + "," + phrase.lower())
            outfile.write("\n")

            if trial % 10 == 0:
                # increment block every 10 trials
                block += 1

                # this tells us that we need to change our accuracy
                if trial % (10*testing_seq_length) == 0:
                    accidx += 1

                    if accidx < len(auto_acc_list):
                        accuracy = auto_acc_list[accidx]

                testing = next(testing_seq)


            trial += 1

        outfile.close()