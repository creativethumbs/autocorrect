1,1,0,0,75,please try to be home before midnight
2,1,0,0,75,a good joke deserves a good laugh
3,1,0,0,75,the early bird gets the worm
4,1,0,0,75,a tumor is okay provided it is benign
5,1,0,0,75,a quarter of a century
6,1,0,0,75,quit while you are ahead
7,1,0,0,75,the fourth edition was better
8,1,0,0,75,you are an ardent capitalist
9,1,0,0,75,do not feel too bad about it
10,1,0,0,75,the treasury department is broke
11,2,1,0,75,no exchange without a bill
12,2,1,0,75,i skimmed through your proposal
13,2,1,0,75,limited warranty of two years
14,2,1,0,75,the bus was very crowded
15,2,1,0,75,please keep this confidential
16,2,1,0,75,the treasurer must balance her books
17,2,1,0,75,time to go shopping
18,2,1,0,75,zero in on the facts
19,2,1,0,75,what a lovely red jacket
20,2,1,0,75,video camera with a zoom lens
21,3,1,0,75,you are not a jedi yet
22,3,1,0,75,fish are jumping
23,3,1,0,75,breathing is difficult
24,3,1,0,75,never too rich and never too thin
25,3,1,0,75,are you talking to me
26,3,1,0,75,the bathroom is good for reading
27,3,1,0,75,sign the withdrawal slip
28,3,1,0,75,please take a bath this month
29,3,1,0,75,we park in driveways
30,3,1,0,75,win first prize in the contest
31,4,0,0,0,i can see the rings on saturn
32,4,0,0,0,the registration period is over
33,4,0,0,0,an inefficient way to heat a house
34,4,0,0,0,my fingers are very cold
35,4,0,0,0,neither a borrower nor a lender be
36,4,0,0,0,join us on the patio
37,4,0,0,0,what goes up must come down
38,4,0,0,0,i just cannot figure this out
39,4,0,0,0,longer than a football field
40,4,0,0,0,head shoulders knees and toes
41,5,1,0,0,the objective of the exercise
42,5,1,0,0,round robin scheduling
43,5,1,0,0,machinery is too complicated
44,5,1,0,0,watch out for low flying objects
45,5,1,0,0,raindrops keep falling on my head
46,5,1,0,0,lie detector tests never work
47,5,1,0,0,if you come home late the doors are locked
48,5,1,0,0,this person is a disaster
49,5,1,0,0,six daughters and seven sons
50,5,1,0,0,everyone wants to win the lottery
51,6,1,0,0,that agreement is rife with problems
52,6,1,0,0,this watch is too expensive
53,6,1,0,0,the dog will bite you
54,6,1,0,0,do not lie in court or else
55,6,1,0,0,fine but only in moderation
56,6,1,0,0,my dog sheds his hair
57,6,1,0,0,dormitory doors are locked at midnight
58,6,1,0,0,your presentation was inspiring
59,6,1,0,0,my favorite sport is racketball
60,6,1,0,0,he cooled off after she left
61,7,0,0,100,do not squander your time
62,7,0,0,100,one of the poorest nations
63,7,0,0,100,irregular verbs are the hardest to learn
64,7,0,0,100,teaching services will help
65,7,0,0,100,we drive on parkways
66,7,0,0,100,burglars never leave their business card
67,7,0,0,100,peering through a small hole
68,7,0,0,100,i like baroque and classical music
69,7,0,0,100,i hate baking pies
70,7,0,0,100,people blow their own horn a lot
71,8,1,0,100,for your information only
72,8,1,0,100,presidents drive expensive cars
73,8,1,0,100,soon we will return from the city
74,8,1,0,100,want to join us for lunch
75,8,1,0,100,the back yard of our house
76,8,1,0,100,communicate through email
77,8,1,0,100,i want to hold your hand
78,8,1,0,100,be persistent to win a strike
79,8,1,0,100,why do you ask silly questions
80,8,1,0,100,interesting observation was made
81,9,1,0,100,i spilled coffee on the carpet
82,9,1,0,100,where can my little dog be
83,9,1,0,100,our life expectancy has increased
84,9,1,0,100,a yard is almost as long as a meter
85,9,1,0,100,the accident scene is a shrine for fans
86,9,1,0,100,apartments are too expensive
87,9,1,0,100,effort is what it will take
88,9,1,0,100,the daring young man
89,9,1,0,100,taking the train is usually faster
90,9,1,0,100,this library has many books
91,10,0,0,25,relations are very strained
92,10,0,0,25,the postal service is very slow
93,10,0,0,25,the water was monitored daily
94,10,0,0,25,i am allergic to bees and peanuts
95,10,0,0,25,there are winners and losers
96,10,0,0,25,historic meeting without a result
97,10,0,0,25,wear a crown with many jewels
98,10,0,0,25,knee bone is connected to the thigh bone
99,10,0,0,25,this is a very good idea
100,10,0,0,25,parking lot is full of trucks
101,11,1,0,25,get rid of that immediately
102,11,1,0,25,i can still feel your presence
103,11,1,0,25,a little encouragement is needed
104,11,1,0,25,that is very unfortunate
105,11,1,0,25,do you like to go camping
106,11,1,0,25,movie about a nutty professor
107,11,1,0,25,no more war no more bloodshed
108,11,1,0,25,in sharp contrast to your words
109,11,1,0,25,he called seven times
110,11,1,0,25,come and see our new car
111,12,1,0,25,very reluctant to enter
112,12,1,0,25,that referendum asked a silly question
113,12,1,0,25,accompanied by an adult
114,12,1,0,25,dolphins leap high out of the water
115,12,1,0,25,a most ridiculous thing
116,12,1,0,25,he is still on our team
117,12,1,0,25,do a good deed to someone
118,12,1,0,25,shivering is one way to keep warm
119,12,1,0,25,all together in one big pile
120,12,1,0,25,travel at the speed of light
121,13,0,0,50,work hard to reach the summit
122,13,0,0,50,safe to walk the streets in the evening
123,13,0,0,50,user friendly interface
124,13,0,0,50,nothing finer than discovering a treasure
125,13,0,0,50,i can play much better now
126,13,0,0,50,players must know all the rules
127,13,0,0,50,for murder you get a long prison sentence
128,13,0,0,50,it should be sunny tomorrow
129,13,0,0,50,tickets are very expensive
130,13,0,0,50,a subject one can really enjoy
131,14,1,0,50,the fire raged for an entire month
132,14,1,0,50,you want to eat your cake
133,14,1,0,50,acutely aware of his good looks
134,14,1,0,50,her majesty visited our country
135,14,1,0,50,my car always breaks in the winter
136,14,1,0,50,i agree with you
137,14,1,0,50,life is but a dream
138,14,1,0,50,a rattle snake is very poisonous
139,14,1,0,50,valid until the end of the year
140,14,1,0,50,spill coffee on the carpet
141,15,1,0,50,can we play cards tonight
142,15,1,0,50,only an idiot would lie in court
143,15,1,0,50,elephants are afraid of mice
144,15,1,0,50,fall is my favorite season
145,15,1,0,50,exercise is good for the mind
146,15,1,0,50,i listen to the tape every day
147,15,1,0,50,freud wrote of the ego
148,15,1,0,50,motivational seminars make me sick
149,15,1,0,50,what to do when the oil runs dry
150,15,1,0,50,the assignment is due today
