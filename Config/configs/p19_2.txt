1,1,0,1,50,join us on the patio
2,1,0,1,50,drove my chevy to the levee
3,1,0,1,50,two or three cups of coffee
4,1,0,1,50,we are subjects and must obey
5,1,0,1,50,a psychiatrist will help you
6,1,0,1,50,earthquakes are predictable
7,1,0,1,50,popularity is desired by all
8,1,0,1,50,the treasurer must balance her books
9,1,0,1,50,traveling requires a lot of fuel
10,1,0,1,50,the first time he tried to swim
11,2,1,1,50,a fox is a very smart animal
12,2,1,1,50,please take a bath this month
13,2,1,1,50,this library has many books
14,2,1,1,50,we park in driveways
15,2,1,1,50,careless driving results in a fine
16,2,1,1,50,the aspirations of a nation
17,2,1,1,50,these barracks are big enough
18,2,1,1,50,important news always seems to be late
19,2,1,1,50,not quite so smart as you think
20,2,1,1,50,the punishment should fit the crime
21,3,1,1,50,a lot of chlorine in the water
22,3,1,1,50,toss the ball around
23,3,1,1,50,bring the offenders to justice
24,3,1,1,50,one hour is allotted for questions
25,3,1,1,50,a dog is the best friend of a man
26,3,1,1,50,the algorithm is too complicated
27,3,1,1,50,zero in on the facts
28,3,1,1,50,dashing through the snow
29,3,1,1,50,parking tickets can be challenged
30,3,1,1,50,i took the rover from the shop
31,4,0,1,75,victims deserve more redress
32,4,0,1,75,a glance in the right direction
33,4,0,1,75,our silver anniversary is coming
34,4,0,1,75,a good response to the question
35,4,0,1,75,employee recruitment takes a lot of effort
36,4,0,1,75,he watched in astonishment
37,4,0,1,75,would you like to come to my house
38,4,0,1,75,double double toil and trouble
39,4,0,1,75,look in the syllabus for the course
40,4,0,1,75,lydia wants to go home
41,5,1,1,75,prescription drugs require a note
42,5,1,1,75,the coronation was very exciting
43,5,1,1,75,i can play much better now
44,5,1,1,75,this equation is too complicated
45,5,1,1,75,irregular verbs are the hardest to learn
46,5,1,1,75,medieval times were very hard
47,5,1,1,75,the force is with you
48,5,1,1,75,great disturbance in the force
49,5,1,1,75,presidents drive expensive cars
50,5,1,1,75,a much higher risk of getting cancer
51,6,1,1,75,the king sends you to the tower
52,6,1,1,75,the water was monitored daily
53,6,1,1,75,acutely aware of his good looks
54,6,1,1,75,a subject one can really enjoy
55,6,1,1,75,everyone wants to win the lottery
56,6,1,1,75,rectangular objects have four sides
57,6,1,1,75,our fax number has changed
58,6,1,1,75,the ropes of a new organization
59,6,1,1,75,construction makes traveling difficult
60,6,1,1,75,a steep learning curve in riding a unicycle
61,7,0,1,25,he is just like everyone else
62,7,0,1,25,good at addition and subtraction
63,7,0,1,25,experience is hard to come by
64,7,0,1,25,a good stimulus deserves a good response
65,7,0,1,25,peek out the window
66,7,0,1,25,win first prize in the contest
67,7,0,1,25,consequences of a wrong turn
68,7,0,1,25,longer than a football field
69,7,0,1,25,the back yard of our house
70,7,0,1,25,keep receipts for all your expenses
71,8,1,1,25,take it to the recycling depot
72,8,1,1,25,well connected with people
73,8,1,1,25,the children are playing
74,8,1,1,25,rent is paid at the beginning of the month
75,8,1,1,25,do not squander your time
76,8,1,1,25,the second largest country
77,8,1,1,25,you will lose your voice
78,8,1,1,25,did you see that spectacular explosion
79,8,1,1,25,the bathroom is good for reading
80,8,1,1,25,one never takes too many precautions
81,9,1,1,25,a big scratch on the tabletop
82,9,1,1,25,motivational seminars make me sick
83,9,1,1,25,universally understood to be wrong
84,9,1,1,25,but the levee was dry
85,9,1,1,25,teaching services will help
86,9,1,1,25,the cotton is high
87,9,1,1,25,i hate baking pies
88,9,1,1,25,rejection letters are discouraging
89,9,1,1,25,a feeling of complete exasperation
90,9,1,1,25,rapidly running short on words
91,10,0,1,100,if diplomacy does not work
92,10,0,1,100,express delivery is very fast
93,10,0,1,100,you are an ardent capitalist
94,10,0,1,100,an injustice is committed every day
95,10,0,1,100,the ventilation system is broken
96,10,0,1,100,february has an extra day
97,10,0,1,100,sit at the front of the bus
98,10,0,1,100,it should be sunny tomorrow
99,10,0,1,100,make up a few more phrases
100,10,0,1,100,the world is a stage
101,11,1,1,100,what a monkey sees a monkey will do
102,11,1,1,100,one heck of a question
103,11,1,1,100,he underwent triple bypass surgery
104,11,1,1,100,my favorite subject is psychology
105,11,1,1,100,the store will close at ten
106,11,1,1,100,interesting observation was made
107,11,1,1,100,the picket line gives me the chills
108,11,1,1,100,movie about a nutty professor
109,11,1,1,100,dormitory doors are locked at midnight
110,11,1,1,100,vanilla flavored ice cream
111,12,1,1,100,so you think you deserve a raise
112,12,1,1,100,electric cars need big fuel cells
113,12,1,1,100,do not worry about this
114,12,1,1,100,a good joke deserves a good laugh
115,12,1,1,100,if you come home late the doors are locked
116,12,1,1,100,prepare for the exam in advance
117,12,1,1,100,the most beautiful sunset
118,12,1,1,100,the acceptance speech was boring
119,12,1,1,100,it is difficult to concentrate
120,12,1,1,100,a yard is almost as long as a meter
121,13,0,1,0,wishful thinking is fine
122,13,0,1,0,soon we will return from the city
123,13,0,1,0,you must make an appointment
124,13,0,1,0,do you prefer a window seat
125,13,0,1,0,burglars never leave their business card
126,13,0,1,0,the proprietor was unavailable
127,13,0,1,0,the voters turfed him out
128,13,0,1,0,watch out for low flying objects
129,13,0,1,0,the winner of the race
130,13,0,1,0,a picture is worth many words
131,14,1,1,0,i am allergic to bees and peanuts
132,14,1,1,0,they love to yap about nothing
133,14,1,1,0,coming up with killer sound bites
134,14,1,1,0,try to enjoy your maternity leave
135,14,1,1,0,a duck quacks to ask for food
136,14,1,1,0,my bare face in the wind
137,14,1,1,0,a thoroughly disgusting thing to say
138,14,1,1,0,the kids are very excited
139,14,1,1,0,beware the ides of march
140,14,1,1,0,a most ridiculous thing
141,15,1,1,0,the dog buried the bone
142,15,1,1,0,come and see our new car
143,15,1,1,0,shall we play a round of cards
144,15,1,1,0,do not drink the water
145,15,1,1,0,mary had a little lamb
146,15,1,1,0,this system of taxation
147,15,1,1,0,learn to walk before you run
148,15,1,1,0,machinery is too complicated
149,15,1,1,0,in sharp contrast to your words
150,15,1,1,0,a rattle snake is very poisonous
