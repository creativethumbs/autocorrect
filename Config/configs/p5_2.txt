1,1,0,1,50,taking the train is usually faster
2,1,0,1,50,rent is paid at the beginning of the month
3,1,0,1,50,take it to the recycling depot
4,1,0,1,50,circumstances are unacceptable
5,1,0,1,50,canada has three territories
6,1,0,1,50,the assignment is due today
7,1,0,1,50,the collapse of the roman empire
8,1,0,1,50,presidents drive expensive cars
9,1,0,1,50,tell a lie and your nose will grow
10,1,0,1,50,can i skate with my sister today
11,2,1,1,50,a dog is the best friend of a man
12,2,1,1,50,give me one spoonful of coffee
13,2,1,1,50,the cream rises to the top
14,2,1,1,50,stay away from strangers
15,2,1,1,50,mystery of the lost lagoon
16,2,1,1,50,a much higher risk of getting cancer
17,2,1,1,50,jumping right out of the water
18,2,1,1,50,elections bring out the best
19,2,1,1,50,the bathroom is good for reading
20,2,1,1,50,do not feel too bad about it
21,3,1,1,50,great disturbance in the force
22,3,1,1,50,that is a very odd question
23,3,1,1,50,this watch is too expensive
24,3,1,1,50,the opposing team is over there
25,3,1,1,50,he is still on our team
26,3,1,1,50,this equation is too complicated
27,3,1,1,50,are you sure you want this
28,3,1,1,50,where did i leave my glasses
29,3,1,1,50,work hard to reach the summit
30,3,1,1,50,all good boys deserve fudge
31,4,0,1,0,the capitol of our nation
32,4,0,1,0,i agree with you
33,4,0,1,0,a touchdown in the last minute
34,4,0,1,0,public transit is much faster
35,4,0,1,0,obligations must be met first
36,4,0,1,0,the dog buried the bone
37,4,0,1,0,join us on the patio
38,4,0,1,0,the minimum amount of time
39,4,0,1,0,i do not care if you do that
40,4,0,1,0,my watch fell in the water
41,5,1,1,0,the facts get in the way
42,5,1,1,0,we have enough witnesses
43,5,1,1,0,the first time he tried to swim
44,5,1,1,0,come and see our new car
45,5,1,1,0,very reluctant to enter
46,5,1,1,0,have a good weekend
47,5,1,1,0,i just cannot figure this out
48,5,1,1,0,just in time for the party
49,5,1,1,0,machinery is too complicated
50,5,1,1,0,teaching services will help
51,6,1,1,0,the punishment should fit the crime
52,6,1,1,0,they love to yap about nothing
53,6,1,1,0,the second largest country
54,6,1,1,0,consequences of a wrong turn
55,6,1,1,0,breathing is difficult
56,6,1,1,0,a good response to the question
57,6,1,1,0,fine but only in moderation
58,6,1,1,0,in sharp contrast to your words
59,6,1,1,0,three two one zero blast off
60,6,1,1,0,this is a non profit organization
61,7,0,1,25,two or three cups of coffee
62,7,0,1,25,the assault took six months
63,7,0,1,25,he called seven times
64,7,0,1,25,interesting observation was made
65,7,0,1,25,an airport is a very busy place
66,7,0,1,25,my bare face in the wind
67,7,0,1,25,thank you for your help
68,7,0,1,25,i hate baking pies
69,7,0,1,25,the cotton is high
70,7,0,1,25,he underwent triple bypass surgery
71,8,1,1,25,love means many things
72,8,1,1,25,go out for some pizza and beer
73,8,1,1,25,the children are playing
74,8,1,1,25,i am allergic to bees and peanuts
75,8,1,1,25,the acceptance speech was boring
76,8,1,1,25,no kissing in the library
77,8,1,1,25,user friendly interface
78,8,1,1,25,february has an extra day
79,8,1,1,25,this person is a disaster
80,8,1,1,25,sign the withdrawal slip
81,9,1,1,25,get rid of that immediately
82,9,1,1,25,a problem with the engine
83,9,1,1,25,questioning the wisdom of the courts
84,9,1,1,25,learn to walk before you run
85,9,1,1,25,this phenomenon will never occur
86,9,1,1,25,employee recruitment takes a lot of effort
87,9,1,1,25,lydia wants to go home
88,9,1,1,25,rapidly running short on words
89,9,1,1,25,luckily my wallet was found
90,9,1,1,25,take a coffee break
91,10,0,1,75,exceed the maximum speed limit
92,10,0,1,75,find a nearby parking spot
93,10,0,1,75,there will be some fog tonight
94,10,0,1,75,quit while you are ahead
95,10,0,1,75,my favorite sport is racketball
96,10,0,1,75,my fingers are very cold
97,10,0,1,75,you want to eat your cake
98,10,0,1,75,where can my little dog be
99,10,0,1,75,starlight and dewdrop
100,10,0,1,75,the imagination of the nation
101,11,1,1,75,please keep this confidential
102,11,1,1,75,do not walk too quickly
103,11,1,1,75,what to do when the oil runs dry
104,11,1,1,75,the algorithm is too complicated
105,11,1,1,75,he cooled off after she left
106,11,1,1,75,this library has many books
107,11,1,1,75,i am wearing a tie and a jacket
108,11,1,1,75,relations are very strained
109,11,1,1,75,we park in driveways
110,11,1,1,75,your etiquette needs some work
111,12,1,1,75,the location of the crime
112,12,1,1,75,do you like to go camping
113,12,1,1,75,the water was monitored daily
114,12,1,1,75,parking lot is full of trucks
115,12,1,1,75,world population is growing
116,12,1,1,75,suburbs are sprawling everywhere
117,12,1,1,75,we went grocery shopping
118,12,1,1,75,the force is with you
119,12,1,1,75,wishful thinking is fine
120,12,1,1,75,did you have a good time
121,13,0,1,100,freud wrote of the ego
122,13,0,1,100,the living is easy
123,13,0,1,100,that sticker needs to be validated
124,13,0,1,100,most judges are very honest
125,13,0,1,100,my bank account is overdrawn
126,13,0,1,100,the stock exchange dipped
127,13,0,1,100,spill coffee on the carpet
128,13,0,1,100,neither a borrower nor a lender be
129,13,0,1,100,the sun rises in the east
130,13,0,1,100,exercise is good for the mind
131,14,1,1,100,universally understood to be wrong
132,14,1,1,100,victims deserve more redress
133,14,1,1,100,the ventilation system is broken
134,14,1,1,100,olympic athletes use drugs
135,14,1,1,100,just what the doctor ordered
136,14,1,1,100,do you like to shop on sunday
137,14,1,1,100,physics and chemistry are hard
138,14,1,1,100,medieval times were very hard
139,14,1,1,100,experience is hard to come by
140,14,1,1,100,travel at the speed of light
141,15,1,1,100,do you prefer a window seat
142,15,1,1,100,put garbage in an abandoned mine
143,15,1,1,100,win first prize in the contest
144,15,1,1,100,the food at this restaurant
145,15,1,1,100,tickets are very expensive
146,15,1,1,100,do not squander your time
147,15,1,1,100,he watched in astonishment
148,15,1,1,100,hair gel is very greasy
149,15,1,1,100,this system of taxation
150,15,1,1,100,the union will go on strike
