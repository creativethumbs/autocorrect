1,1,0,0,25,i do not care if you do that
2,1,0,0,25,you have my sympathy
3,1,0,0,25,that referendum asked a silly question
4,1,0,0,25,fish are jumping
5,1,0,0,25,the ropes of a new organization
6,1,0,0,25,movie about a nutty professor
7,1,0,0,25,buckle up for safety
8,1,0,0,25,my preferred treat is chocolate
9,1,0,0,25,if you come home late the doors are locked
10,1,0,0,25,sing the gospel and the blues
11,2,1,0,25,i just cannot figure this out
12,2,1,0,25,this watch is too expensive
13,2,1,0,25,the king sends you to the tower
14,2,1,0,25,knee bone is connected to the thigh bone
15,2,1,0,25,experience is hard to come by
16,2,1,0,25,travel at the speed of light
17,2,1,0,25,information super highway
18,2,1,0,25,time to go shopping
19,2,1,0,25,for your information only
20,2,1,0,25,the laser printer is jammed
21,3,1,0,25,you should visit a doctor
22,3,1,0,25,the algorithm is too complicated
23,3,1,0,25,an excellent way to communicate
24,3,1,0,25,a dog is the best friend of a man
25,3,1,0,25,only an idiot would lie in court
26,3,1,0,25,every apple from every tree
27,3,1,0,25,a security force of eight thousand
28,3,1,0,25,the dog buried the bone
29,3,1,0,25,the price of gas is high
30,3,1,0,25,i like baroque and classical music
31,4,0,0,50,please follow the guidelines
32,4,0,0,50,sign the withdrawal slip
33,4,0,0,50,motivational seminars make me sick
34,4,0,0,50,tell a lie and your nose will grow
35,4,0,0,50,thank you for your help
36,4,0,0,50,superman never wore a mask
37,4,0,0,50,go out for some pizza and beer
38,4,0,0,50,gun powder must be handled with care
39,4,0,0,50,wishful thinking is fine
40,4,0,0,50,stability of the nation
41,5,1,0,50,did you see that spectacular explosion
42,5,1,0,50,the fourth edition was better
43,5,1,0,50,breathing is difficult
44,5,1,0,50,presidents drive expensive cars
45,5,1,0,50,limited warranty of two years
46,5,1,0,50,are you sure you want this
47,5,1,0,50,a subject one can really enjoy
48,5,1,0,50,file all complaints in writing
49,5,1,0,50,i agree with you
50,5,1,0,50,wear a crown with many jewels
51,6,1,0,50,house with new electrical panel
52,6,1,0,50,just what the doctor ordered
53,6,1,0,50,the chamber makes important decisions
54,6,1,0,50,that sticker needs to be validated
55,6,1,0,50,chemical spill took forever
56,6,1,0,50,they watched the entire movie
57,6,1,0,50,my mother makes good cookies
58,6,1,0,50,soon we will return from the city
59,6,1,0,50,please try to be home before midnight
60,6,1,0,50,parking tickets can be challenged
61,7,0,0,0,learn to walk before you run
62,7,0,0,0,the living is easy
63,7,0,0,0,keep receipts for all your expenses
64,7,0,0,0,where did i leave my glasses
65,7,0,0,0,careless driving results in a fine
66,7,0,0,0,the rationale behind the decision
67,7,0,0,0,i can still feel your presence
68,7,0,0,0,a feeling of complete exasperation
69,7,0,0,0,i like to play tennis
70,7,0,0,0,get your priorities in order
71,8,1,0,0,every saturday he folds the laundry
72,8,1,0,0,we accept personal checks
73,8,1,0,0,this camera takes nice photographs
74,8,1,0,0,do not lie in court or else
75,8,1,0,0,the coronation was very exciting
76,8,1,0,0,suburbs are sprawling everywhere
77,8,1,0,0,with each step forward
78,8,1,0,0,the pen is mightier than the sword
79,8,1,0,0,consequences of a wrong turn
80,8,1,0,0,the imagination of the nation
81,9,1,0,0,protect your environment
82,9,1,0,0,my fingers are very cold
83,9,1,0,0,canada has ten provinces
84,9,1,0,0,coming up with killer sound bites
85,9,1,0,0,construction makes traveling difficult
86,9,1,0,0,he is just like everyone else
87,9,1,0,0,the early bird gets the worm
88,9,1,0,0,the stock exchange dipped
89,9,1,0,0,you want to eat your cake
90,9,1,0,0,universally understood to be wrong
91,10,0,0,100,the objective of the exercise
92,10,0,0,100,public transit is much faster
93,10,0,0,100,prevailing wind from the east
94,10,0,0,100,peering through a small hole
95,10,0,0,100,interesting observation was made
96,10,0,0,100,the punishment should fit the crime
97,10,0,0,100,i listen to the tape every day
98,10,0,0,100,see you later alligator
99,10,0,0,100,on the way to the cottage
100,10,0,0,100,we are subjects and must obey
101,11,1,0,100,head shoulders knees and toes
102,11,1,0,100,an occasional taste of chocolate
103,11,1,0,100,rectangular objects have four sides
104,11,1,0,100,there will be some fog tonight
105,11,1,0,100,relations are very strained
106,11,1,0,100,a thoroughly disgusting thing to say
107,11,1,0,100,the store will close at ten
108,11,1,0,100,the collapse of the roman empire
109,11,1,0,100,that is very unfortunate
110,11,1,0,100,one heck of a question
111,12,1,0,100,frequently asked questions
112,12,1,0,100,communicate through email
113,12,1,0,100,space is a high priority
114,12,1,0,100,the treasurer must balance her books
115,12,1,0,100,please provide your date of birth
116,12,1,0,100,the sum of the parts
117,12,1,0,100,question that must be answered
118,12,1,0,100,the library is closed today
119,12,1,0,100,staying up all night is a bad idea
120,12,1,0,100,good jobs for those with education
121,13,0,0,75,starlight and dewdrop
122,13,0,0,75,a good response to the question
123,13,0,0,75,bring the offenders to justice
124,13,0,0,75,handicapped persons need consideration
125,13,0,0,75,prescription drugs require a note
126,13,0,0,75,that is a very odd question
127,13,0,0,75,six daughters and seven sons
128,13,0,0,75,a steep learning curve in riding a unicycle
129,13,0,0,75,quit while you are ahead
130,13,0,0,75,my car always breaks in the winter
131,14,1,0,75,double double toil and trouble
132,14,1,0,75,look in the syllabus for the course
133,14,1,0,75,a good joke deserves a good laugh
134,14,1,0,75,what a monkey sees a monkey will do
135,14,1,0,75,apartments are too expensive
136,14,1,0,75,what a lovely red jacket
137,14,1,0,75,our fax number has changed
138,14,1,0,75,beautiful paintings in the gallery
139,14,1,0,75,the picket line gives me the chills
140,14,1,0,75,the second largest country
141,15,1,0,75,historic meeting without a result
142,15,1,0,75,the ventilation system is broken
143,15,1,0,75,hair gel is very greasy
144,15,1,0,75,nobody cares anymore
145,15,1,0,75,machinery is too complicated
146,15,1,0,75,the back yard of our house
147,15,1,0,75,i hate baking pies
148,15,1,0,75,nothing finer than discovering a treasure
149,15,1,0,75,there are winners and losers
150,15,1,0,75,bank transaction was not registered
