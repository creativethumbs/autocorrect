1,1,0,1,0,i will meet you at noon
2,1,0,1,0,the presidential suite is very busy
3,1,0,1,0,wear a crown with many jewels
4,1,0,1,0,my mother makes good cookies
5,1,0,1,0,tell a lie and your nose will grow
6,1,0,1,0,rapidly running short on words
7,1,0,1,0,sit at the front of the bus
8,1,0,1,0,the opposing team is over there
9,1,0,1,0,handicapped persons need consideration
10,1,0,1,0,chemical spill took forever
11,2,1,1,0,apartments are too expensive
12,2,1,1,0,this camera takes nice photographs
13,2,1,1,0,flashing red light means stop
14,2,1,1,0,safe to walk the streets in the evening
15,2,1,1,0,a picture is worth many words
16,2,1,1,0,the trains are always late
17,2,1,1,0,weeping willows are found near water
18,2,1,1,0,motivational seminars make me sick
19,2,1,1,0,he watched in astonishment
20,2,1,1,0,do not say anything
21,3,1,1,0,prescription drugs require a note
22,3,1,1,0,just in time for the party
23,3,1,1,0,exercise is good for the mind
24,3,1,1,0,the assault took six months
25,3,1,1,0,soon we will return from the city
26,3,1,1,0,santa claus got stuck
27,3,1,1,0,take it to the recycling depot
28,3,1,1,0,all work and no play
29,3,1,1,0,seasoned golfers love the game
30,3,1,1,0,you want to eat your cake
31,4,0,1,100,stay away from strangers
32,4,0,1,100,one never takes too many precautions
33,4,0,1,100,important for political parties
34,4,0,1,100,do not drink too much
35,4,0,1,100,my preferred treat is chocolate
36,4,0,1,100,my dog sheds his hair
37,4,0,1,100,healthy food is good for you
38,4,0,1,100,time to go shopping
39,4,0,1,100,just what the doctor ordered
40,4,0,1,100,sing the gospel and the blues
41,5,1,1,100,the cream rises to the top
42,5,1,1,100,victims deserve more redress
43,5,1,1,100,starlight and dewdrop
44,5,1,1,100,watch out for low flying objects
45,5,1,1,100,destruction of the rain forest
46,5,1,1,100,try to enjoy your maternity leave
47,5,1,1,100,the algorithm is too complicated
48,5,1,1,100,this library has many books
49,5,1,1,100,the chamber makes important decisions
50,5,1,1,100,every apple from every tree
51,6,1,1,100,this is a very good idea
52,6,1,1,100,house with new electrical panel
53,6,1,1,100,the accident scene is a shrine for fans
54,6,1,1,100,the imagination of the nation
55,6,1,1,100,pumping brakes when roads are slippery
56,6,1,1,100,the ropes of a new organization
57,6,1,1,100,have a good weekend
58,6,1,1,100,hair gel is very greasy
59,6,1,1,100,express delivery is very fast
60,6,1,1,100,burglars never leave their business card
61,7,0,1,25,parking tickets can be challenged
62,7,0,1,25,the quick brown fox jumped
63,7,0,1,25,the coronation was very exciting
64,7,0,1,25,a much higher risk of getting cancer
65,7,0,1,25,toss the ball around
66,7,0,1,25,a rattle snake is very poisonous
67,7,0,1,25,sharp cheese keeps the mind sharp
68,7,0,1,25,she wears too much makeup
69,7,0,1,25,popularity is desired by all
70,7,0,1,25,my favorite subject is psychology
71,8,1,1,25,this watch is too expensive
72,8,1,1,25,the assignment is due today
73,8,1,1,25,we park in driveways
74,8,1,1,25,i like to play tennis
75,8,1,1,25,consequences of a wrong turn
76,8,1,1,25,batman wears a cape
77,8,1,1,25,rent is paid at the beginning of the month
78,8,1,1,25,the bathroom is good for reading
79,8,1,1,25,work hard to reach the summit
80,8,1,1,25,you are an ardent capitalist
81,9,1,1,25,our fax number has changed
82,9,1,1,25,be persistent to win a strike
83,9,1,1,25,it is very windy today
84,9,1,1,25,we run the risk of failure
85,9,1,1,25,stability of the nation
86,9,1,1,25,are you talking to me
87,9,1,1,25,electric cars need big fuel cells
88,9,1,1,25,only an idiot would lie in court
89,9,1,1,25,the chancellor was very boring
90,9,1,1,25,dashing through the snow
91,10,0,1,50,if you come home late the doors are locked
92,10,0,1,50,the biggest hamburger i have ever seen
93,10,0,1,50,what you see is what you get
94,10,0,1,50,historic meeting without a result
95,10,0,1,50,do not lie in court or else
96,10,0,1,50,thank you for your help
97,10,0,1,50,gamblers eventually lose their shirts
98,10,0,1,50,do not worry about this
99,10,0,1,50,interactions between men and women
100,10,0,1,50,salesmen must make their monthly quota
101,11,1,1,50,obligations must be met first
102,11,1,1,50,the force is with you
103,11,1,1,50,are you sure you want this
104,11,1,1,50,do not squander your time
105,11,1,1,50,a psychiatrist will help you
106,11,1,1,50,the punishment should fit the crime
107,11,1,1,50,an injustice is committed every day
108,11,1,1,50,win first prize in the contest
109,11,1,1,50,insurance is important for bad drivers
110,11,1,1,50,circumstances are unacceptable
111,12,1,1,50,we accept personal checks
112,12,1,1,50,if diplomacy does not work
113,12,1,1,50,please provide your date of birth
114,12,1,1,50,do not drink the water
115,12,1,1,50,never mix religion and politics
116,12,1,1,50,space is a high priority
117,12,1,1,50,the postal service is very slow
118,12,1,1,50,an inefficient way to heat a house
119,12,1,1,50,for murder you get a long prison sentence
120,12,1,1,50,my car always breaks in the winter
121,13,0,1,75,he underwent triple bypass surgery
122,13,0,1,75,quit while you are ahead
123,13,0,1,75,get your priorities in order
124,13,0,1,75,a feeling of complete exasperation
125,13,0,1,75,an offer you cannot refuse
126,13,0,1,75,sent this by registered mail
127,13,0,1,75,give me one spoonful of coffee
128,13,0,1,75,spill coffee on the carpet
129,13,0,1,75,canada has three territories
130,13,0,1,75,raindrops keep falling on my head
131,14,1,1,75,we have enough witnesses
132,14,1,1,75,the fire blazed all weekend
133,14,1,1,75,go out for some pizza and beer
134,14,1,1,75,for your information only
135,14,1,1,75,knee bone is connected to the thigh bone
136,14,1,1,75,wishful thinking is fine
137,14,1,1,75,please keep this confidential
138,14,1,1,75,beautiful paintings in the gallery
139,14,1,1,75,a lot of chlorine in the water
140,14,1,1,75,the sun rises in the east
141,15,1,1,75,that is a very odd question
142,15,1,1,75,suburbs are sprawling everywhere
143,15,1,1,75,parking lot is full of trucks
144,15,1,1,75,buckle up for safety
145,15,1,1,75,round robin scheduling
146,15,1,1,75,do not feel too bad about it
147,15,1,1,75,this phenomenon will never occur
148,15,1,1,75,listen to five hours of opera
149,15,1,1,75,file all complaints in writing
150,15,1,1,75,look in the syllabus for the course
