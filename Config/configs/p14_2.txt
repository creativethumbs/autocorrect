1,1,0,0,75,a picture is worth many words
2,1,0,0,75,consequences of a wrong turn
3,1,0,0,75,mom made her a turtleneck
4,1,0,0,75,fine but only in moderation
5,1,0,0,75,can we play cards tonight
6,1,0,0,75,the biggest hamburger i have ever seen
7,1,0,0,75,my favorite web browser
8,1,0,0,75,my car always breaks in the winter
9,1,0,0,75,on the way to the cottage
10,1,0,0,75,gun powder must be handled with care
11,2,1,0,75,drove my chevy to the levee
12,2,1,0,75,nobody cares anymore
13,2,1,0,75,elections bring out the best
14,2,1,0,75,the coronation was very exciting
15,2,1,0,75,quick there is someone knocking
16,2,1,0,75,taking the train is usually faster
17,2,1,0,75,salesmen must make their monthly quota
18,2,1,0,75,i watched blazing saddles
19,2,1,0,75,i agree with you
20,2,1,0,75,mary had a little lamb
21,3,1,0,75,breathing is difficult
22,3,1,0,75,a good response to the question
23,3,1,0,75,information super highway
24,3,1,0,75,a lot of chlorine in the water
25,3,1,0,75,rapidly running short on words
26,3,1,0,75,a dog is the best friend of a man
27,3,1,0,75,video camera with a zoom lens
28,3,1,0,75,he watched in astonishment
29,3,1,0,75,traveling requires a lot of fuel
30,3,1,0,75,it looks like a shack
31,4,0,0,50,two or three cups of coffee
32,4,0,0,50,machinery is too complicated
33,4,0,0,50,you should visit a doctor
34,4,0,0,50,hands on experience with a job
35,4,0,0,50,i like baroque and classical music
36,4,0,0,50,the protesters blocked all traffic
37,4,0,0,50,drugs should be avoided
38,4,0,0,50,sad to hear that news
39,4,0,0,50,do not lie in court or else
40,4,0,0,50,earthquakes are predictable
41,5,1,0,50,canada has ten provinces
42,5,1,0,50,we better investigate this
43,5,1,0,50,a psychiatrist will help you
44,5,1,0,50,players must know all the rules
45,5,1,0,50,i will meet you at noon
46,5,1,0,50,the cream rises to the top
47,5,1,0,50,soon we will return from the city
48,5,1,0,50,learn to walk before you run
49,5,1,0,50,we went grocery shopping
50,5,1,0,50,we are subjects and must obey
51,6,1,0,50,apartments are too expensive
52,6,1,0,50,it is very windy today
53,6,1,0,50,give me one spoonful of coffee
54,6,1,0,50,the fire blazed all weekend
55,6,1,0,50,questioning the wisdom of the courts
56,6,1,0,50,thank you for your help
57,6,1,0,50,valium in the economy size
58,6,1,0,50,we park in driveways
59,6,1,0,50,my favorite sport is racketball
60,6,1,0,50,universities are too expensive
61,7,0,0,25,if at first you do not succeed
62,7,0,0,25,the chamber makes important decisions
63,7,0,0,25,an injustice is committed every day
64,7,0,0,25,the plug does not fit the socket
65,7,0,0,25,a quarter of a century
66,7,0,0,25,one never takes too many precautions
67,7,0,0,25,one of the poorest nations
68,7,0,0,25,the voters turfed him out
69,7,0,0,25,wishful thinking is fine
70,7,0,0,25,just in time for the party
71,8,1,0,25,a feeling of complete exasperation
72,8,1,0,25,circumstances are unacceptable
73,8,1,0,25,universally understood to be wrong
74,8,1,0,25,file all complaints in writing
75,8,1,0,25,all work and no play
76,8,1,0,25,i spilled coffee on the carpet
77,8,1,0,25,listen to five hours of opera
78,8,1,0,25,the punishment should fit the crime
79,8,1,0,25,if diplomacy does not work
80,8,1,0,25,the opposing team is over there
81,9,1,0,25,the ropes of a new organization
82,9,1,0,25,a big scratch on the tabletop
83,9,1,0,25,we dine out on the weekends
84,9,1,0,25,lydia wants to go home
85,9,1,0,25,a tumor is okay provided it is benign
86,9,1,0,25,mystery of the lost lagoon
87,9,1,0,25,no exchange without a bill
88,9,1,0,25,the accident scene is a shrine for fans
89,9,1,0,25,correct your diction immediately
90,9,1,0,25,that sticker needs to be validated
91,10,0,0,0,a touchdown in the last minute
92,10,0,0,0,this person is a disaster
93,10,0,0,0,the quick brown fox jumped
94,10,0,0,0,parking lot is full of trucks
95,10,0,0,0,you will lose your voice
96,10,0,0,0,the four seasons will come
97,10,0,0,0,this equation is too complicated
98,10,0,0,0,we missed your birthday
99,10,0,0,0,beware the ides of march
100,10,0,0,0,is there any indication of this
101,11,1,0,0,the treasurer must balance her books
102,11,1,0,0,batman wears a cape
103,11,1,0,0,the winner of the race
104,11,1,0,0,dashing through the snow
105,11,1,0,0,the high waves will swamp us
106,11,1,0,0,the presidential suite is very busy
107,11,1,0,0,did you have a good time
108,11,1,0,0,i will put on my glasses
109,11,1,0,0,spill coffee on the carpet
110,11,1,0,0,that referendum asked a silly question
111,12,1,0,0,express delivery is very fast
112,12,1,0,0,i took the rover from the shop
113,12,1,0,0,question that must be answered
114,12,1,0,0,every saturday he folds the laundry
115,12,1,0,0,what a lovely red jacket
116,12,1,0,0,the king sends you to the tower
117,12,1,0,0,longer than a football field
118,12,1,0,0,gamblers eventually lose their shirts
119,12,1,0,0,i skimmed through your proposal
120,12,1,0,0,take a coffee break
121,13,0,0,100,only an idiot would lie in court
122,13,0,0,100,protect your environment
123,13,0,0,100,the water was monitored daily
124,13,0,0,100,the capitol of our nation
125,13,0,0,100,peek out the window
126,13,0,0,100,everyone wants to win the lottery
127,13,0,0,100,have a good weekend
128,13,0,0,100,my dog sheds his hair
129,13,0,0,100,the music is better than it sounds
130,13,0,0,100,the children are playing
131,14,1,0,100,prevailing wind from the east
132,14,1,0,100,what goes up must come down
133,14,1,0,100,want to join us for lunch
134,14,1,0,100,historic meeting without a result
135,14,1,0,100,the back yard of our house
136,14,1,0,100,are you sure you want this
137,14,1,0,100,no kissing in the library
138,14,1,0,100,goldilocks and the three bears
139,14,1,0,100,fish are jumping
140,14,1,0,100,do you prefer a window seat
141,15,1,0,100,you must make an appointment
142,15,1,0,100,your presentation was inspiring
143,15,1,0,100,safe to walk the streets in the evening
144,15,1,0,100,the ventilation system is broken
145,15,1,0,100,it is difficult to concentrate
146,15,1,0,100,he is still on our team
147,15,1,0,100,sign the withdrawal slip
148,15,1,0,100,i do not care if you do that
149,15,1,0,100,get aboard the ship is leaving
150,15,1,0,100,this is too much to handle
