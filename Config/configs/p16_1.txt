1,1,0,1,75,stay away from strangers
2,1,0,1,75,my dog sheds his hair
3,1,0,1,75,we park in driveways
4,1,0,1,75,my fingers are very cold
5,1,0,1,75,the proprietor was unavailable
6,1,0,1,75,we accept personal checks
7,1,0,1,75,universally understood to be wrong
8,1,0,1,75,i hate baking pies
9,1,0,1,75,a tumor is okay provided it is benign
10,1,0,1,75,he watched in astonishment
11,2,1,1,75,the voters turfed him out
12,2,1,1,75,the assault took six months
13,2,1,1,75,chemical spill took forever
14,2,1,1,75,get rid of that immediately
15,2,1,1,75,dashing through the snow
16,2,1,1,75,sit at the front of the bus
17,2,1,1,75,that is very unfortunate
18,2,1,1,75,the punishment should fit the crime
19,2,1,1,75,nobody cares anymore
20,2,1,1,75,the music is better than it sounds
21,3,1,1,75,a touchdown in the last minute
22,3,1,1,75,they love to yap about nothing
23,3,1,1,75,the coronation was very exciting
24,3,1,1,75,the presidential suite is very busy
25,3,1,1,75,prayer in schools offends some
26,3,1,1,75,healthy food is good for you
27,3,1,1,75,gas bills are sent monthly
28,3,1,1,75,the dog will bite you
29,3,1,1,75,coalition governments never work
30,3,1,1,75,earthquakes are predictable
31,4,0,1,0,the most beautiful sunset
32,4,0,1,0,never mix religion and politics
33,4,0,1,0,vanilla flavored ice cream
34,4,0,1,0,please try to be home before midnight
35,4,0,1,0,companies announce a merger
36,4,0,1,0,yes you are very smart
37,4,0,1,0,medieval times were very hard
38,4,0,1,0,a duck quacks to ask for food
39,4,0,1,0,are you sure you want this
40,4,0,1,0,there will be some fog tonight
41,5,1,1,0,the largest of the five oceans
42,5,1,1,0,the acceptance speech was boring
43,5,1,1,0,what a lovely red jacket
44,5,1,1,0,sing the gospel and the blues
45,5,1,1,0,important for political parties
46,5,1,1,0,the cream rises to the top
47,5,1,1,0,see you later alligator
48,5,1,1,0,apartments are too expensive
49,5,1,1,0,do not feel too bad about it
50,5,1,1,0,effort is what it will take
51,6,1,1,0,that agreement is rife with problems
52,6,1,1,0,double double toil and trouble
53,6,1,1,0,sent this by registered mail
54,6,1,1,0,a coupon for a free sample
55,6,1,1,0,exceed the maximum speed limit
56,6,1,1,0,drugs should be avoided
57,6,1,1,0,a picture is worth many words
58,6,1,1,0,the objective of the exercise
59,6,1,1,0,express delivery is very fast
60,6,1,1,0,just in time for the party
61,7,0,1,50,her majesty visited our country
62,7,0,1,50,rain rain go away
63,7,0,1,50,the food at this restaurant
64,7,0,1,50,wishful thinking is fine
65,7,0,1,50,if you come home late the doors are locked
66,7,0,1,50,we went grocery shopping
67,7,0,1,50,get your priorities in order
68,7,0,1,50,the price of gas is high
69,7,0,1,50,a most ridiculous thing
70,7,0,1,50,traveling to conferences is fun
71,8,1,1,50,i like to play tennis
72,8,1,1,50,on the way to the cottage
73,8,1,1,50,one never takes too many precautions
74,8,1,1,50,the fax machine is broken
75,8,1,1,50,quick there is someone knocking
76,8,1,1,50,an airport is a very busy place
77,8,1,1,50,the rationale behind the decision
78,8,1,1,50,a good response to the question
79,8,1,1,50,parking lot is full of trucks
80,8,1,1,50,parking tickets can be challenged
81,9,1,1,50,the gun discharged by accident
82,9,1,1,50,do not squander your time
83,9,1,1,50,insurance is important for bad drivers
84,9,1,1,50,the chamber makes important decisions
85,9,1,1,50,call for more details
86,9,1,1,50,people blow their own horn a lot
87,9,1,1,50,the insulation is not working
88,9,1,1,50,the water was monitored daily
89,9,1,1,50,i will meet you at noon
90,9,1,1,50,take a coffee break
91,10,0,1,25,machinery is too complicated
92,10,0,1,25,mystery of the lost lagoon
93,10,0,1,25,bad for the environment
94,10,0,1,25,do not say anything
95,10,0,1,25,this camera takes nice photographs
96,10,0,1,25,where can my little dog be
97,10,0,1,25,be home before midnight
98,10,0,1,25,he is shouting loudly
99,10,0,1,25,i will put on my glasses
100,10,0,1,25,the protesters blocked all traffic
101,11,1,1,25,i just cannot figure this out
102,11,1,1,25,communicate through email
103,11,1,1,25,time to go shopping
104,11,1,1,25,make up a few more phrases
105,11,1,1,25,he played a hero in that movie
106,11,1,1,25,sharp cheese keeps the mind sharp
107,11,1,1,25,staying up all night is a bad idea
108,11,1,1,25,work hard to reach the summit
109,11,1,1,25,not quite so smart as you think
110,11,1,1,25,always cover all the bases
111,12,1,1,25,a lot of chlorine in the water
112,12,1,1,25,do not lie in court or else
113,12,1,1,25,a quarter of a century
114,12,1,1,25,flashing red light means stop
115,12,1,1,25,my favorite subject is psychology
116,12,1,1,25,rent is paid at the beginning of the month
117,12,1,1,25,with each step forward
118,12,1,1,25,they might find your comment offensive
119,12,1,1,25,i watched blazing saddles
120,12,1,1,25,we drive on parkways
121,13,0,1,100,vote according to your conscience
122,13,0,1,100,the capitol of our nation
123,13,0,1,100,the fire blazed all weekend
124,13,0,1,100,just like it says on the canned good
125,13,0,1,100,goldilocks and the three bears
126,13,0,1,100,just what the doctor ordered
127,13,0,1,100,come and see our new car
128,13,0,1,100,mom made her a turtleneck
129,13,0,1,100,sign the withdrawal slip
130,13,0,1,100,neither a borrower nor a lender be
131,14,1,1,100,the quick brown fox jumped
132,14,1,1,100,it is very windy today
133,14,1,1,100,this person is a disaster
134,14,1,1,100,the generation gap gets wider
135,14,1,1,100,the force is with you
136,14,1,1,100,faster than a speeding bullet
137,14,1,1,100,saving that child was a heroic effort
138,14,1,1,100,all good boys deserve fudge
139,14,1,1,100,information super highway
140,14,1,1,100,for murder you get a long prison sentence
141,15,1,1,100,teaching services will help
142,15,1,1,100,it should be sunny tomorrow
143,15,1,1,100,prepare for the exam in advance
144,15,1,1,100,the bathroom is good for reading
145,15,1,1,100,keep receipts for all your expenses
146,15,1,1,100,take it to the recycling depot
147,15,1,1,100,that is a very nasty cut
148,15,1,1,100,the daring young man
149,15,1,1,100,salesmen must make their monthly quota
150,15,1,1,100,you must make an appointment
