1,1,0,2,0,shall we play a round of cards
2,1,0,2,0,the daring young man
3,1,0,2,0,an occasional taste of chocolate
4,1,0,2,0,you must make an appointment
5,1,0,2,0,mystery of the lost lagoon
6,1,0,2,0,play it again sam
7,1,0,2,0,physics and chemistry are hard
8,1,0,2,0,healthy food is good for you
9,1,0,2,0,a most ridiculous thing
10,1,0,2,0,breathing is difficult
11,2,1,2,0,beautiful paintings in the gallery
12,2,1,2,0,be home before midnight
13,2,1,2,0,double double toil and trouble
14,2,1,2,0,all together in one big pile
15,2,1,2,0,bad for the environment
16,2,1,2,0,most judges are very honest
17,2,1,2,0,apartments are too expensive
18,2,1,2,0,elephants are afraid of mice
19,2,1,2,0,a good stimulus deserves a good response
20,2,1,2,0,he is just like everyone else
21,3,1,2,0,i am wearing a tie and a jacket
22,3,1,2,0,make up a few more phrases
23,3,1,2,0,a touchdown in the last minute
24,3,1,2,0,we run the risk of failure
25,3,1,2,0,movie about a nutty professor
26,3,1,2,0,an inefficient way to heat a house
27,3,1,2,0,traveling requires a lot of fuel
28,3,1,2,0,if at first you do not succeed
29,3,1,2,0,i like to play tennis
30,3,1,2,0,coming up with killer sound bites
31,4,0,2,25,your etiquette needs some work
32,4,0,2,25,just like it says on the canned good
33,4,0,2,25,buckle up for safety
34,4,0,2,25,the fax machine is broken
35,4,0,2,25,this watch is too expensive
36,4,0,2,25,wishful thinking is fine
37,4,0,2,25,toss the ball around
38,4,0,2,25,the aspirations of a nation
39,4,0,2,25,the fourth edition was better
40,4,0,2,25,we better investigate this
41,5,1,2,25,freud wrote of the ego
42,5,1,2,25,pay off a mortgage for a house
43,5,1,2,25,there are winners and losers
44,5,1,2,25,a good response to the question
45,5,1,2,25,we have enough witnesses
46,5,1,2,25,all work and no play
47,5,1,2,25,life is but a dream
48,5,1,2,25,batman wears a cape
49,5,1,2,25,labor unions know how to organize
50,5,1,2,25,rectangular objects have four sides
51,6,1,2,25,no kissing in the library
52,6,1,2,25,you are not a jedi yet
53,6,1,2,25,coalition governments never work
54,6,1,2,25,the power of denial
55,6,1,2,25,i can play much better now
56,6,1,2,25,what a monkey sees a monkey will do
57,6,1,2,25,circumstances are unacceptable
58,6,1,2,25,our silver anniversary is coming
59,6,1,2,25,do not drink too much
60,6,1,2,25,look in the syllabus for the course
61,7,0,2,75,an airport is a very busy place
62,7,0,2,75,hands on experience with a job
63,7,0,2,75,the rationale behind the decision
64,7,0,2,75,vanilla flavored ice cream
65,7,0,2,75,lie detector tests never work
66,7,0,2,75,an injustice is committed every day
67,7,0,2,75,drove my chevy to the levee
68,7,0,2,75,go out for some pizza and beer
69,7,0,2,75,watch out for low flying objects
70,7,0,2,75,world population is growing
71,8,1,2,75,file all complaints in writing
72,8,1,2,75,rain rain go away
73,8,1,2,75,quit while you are ahead
74,8,1,2,75,he was wearing a sweatshirt
75,8,1,2,75,the ropes of a new organization
76,8,1,2,75,a psychiatrist will help you
77,8,1,2,75,drugs should be avoided
78,8,1,2,75,these barracks are big enough
79,8,1,2,75,a correction had to be published
80,8,1,2,75,the protesters blocked all traffic
81,9,1,2,75,our housekeeper does a thorough job
82,9,1,2,75,knee bone is connected to the thigh bone
83,9,1,2,75,the treasury department is broke
84,9,1,2,75,interactions between men and women
85,9,1,2,75,goldilocks and the three bears
86,9,1,2,75,this system of taxation
87,9,1,2,75,santa claus got stuck
88,9,1,2,75,well connected with people
89,9,1,2,75,machinery is too complicated
90,9,1,2,75,this person is a disaster
91,10,0,2,100,bank transaction was not registered
92,10,0,2,100,electric cars need big fuel cells
93,10,0,2,100,shivering is one way to keep warm
94,10,0,2,100,one of the poorest nations
95,10,0,2,100,people blow their own horn a lot
96,10,0,2,100,reading week is just about here
97,10,0,2,100,he called seven times
98,10,0,2,100,just in time for the party
99,10,0,2,100,that is a very nasty cut
100,10,0,2,100,bring the offenders to justice
101,11,1,2,100,put garbage in an abandoned mine
102,11,1,2,100,this is a very good idea
103,11,1,2,100,i want to hold your hand
104,11,1,2,100,come and see our new car
105,11,1,2,100,the price of gas is high
106,11,1,2,100,prepare for the exam in advance
107,11,1,2,100,where can my little dog be
108,11,1,2,100,mary had a little lamb
109,11,1,2,100,microscopes make small things look big
110,11,1,2,100,please keep this confidential
111,12,1,2,100,make my day you sucker
112,12,1,2,100,we dine out on the weekends
113,12,1,2,100,a fox is a very smart animal
114,12,1,2,100,the punishment should fit the crime
115,12,1,2,100,accompanied by an adult
116,12,1,2,100,acutely aware of his good looks
117,12,1,2,100,you should visit a doctor
118,12,1,2,100,pumping brakes when roads are slippery
119,12,1,2,100,obligations must be met first
120,12,1,2,100,parking tickets can be challenged
121,13,0,2,50,no exchange without a bill
122,13,0,2,50,we park in driveways
123,13,0,2,50,you have my sympathy
124,13,0,2,50,i just cannot figure this out
125,13,0,2,50,these cookies are so amazing
126,13,0,2,50,prayer in schools offends some
127,13,0,2,50,the assignment is due today
128,13,0,2,50,the elevator door appears to be stuck
129,13,0,2,50,this leather jacket is too warm
130,13,0,2,50,an excellent way to communicate
131,14,1,2,50,a yard is almost as long as a meter
132,14,1,2,50,he is shouting loudly
133,14,1,2,50,the sum of the parts
134,14,1,2,50,handicapped persons need consideration
135,14,1,2,50,every saturday he folds the laundry
136,14,1,2,50,universities are too expensive
137,14,1,2,50,sing the gospel and the blues
138,14,1,2,50,do not walk too quickly
139,14,1,2,50,fall is my favorite season
140,14,1,2,50,the collapse of the roman empire
141,15,1,2,50,suburbs are sprawling everywhere
142,15,1,2,50,this is a non profit organization
143,15,1,2,50,communicate through email
144,15,1,2,50,rent is paid at the beginning of the month
145,15,1,2,50,a thoroughly disgusting thing to say
146,15,1,2,50,the union will go on strike
147,15,1,2,50,fish are jumping
148,15,1,2,50,i took the rover from the shop
149,15,1,2,50,the cotton is high
150,15,1,2,50,effort is what it will take
