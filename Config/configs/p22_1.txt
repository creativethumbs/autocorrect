1,1,0,1,0,handicapped persons need consideration
2,1,0,1,0,superman never wore a mask
3,1,0,1,0,do you like to go camping
4,1,0,1,0,arguing with the boss is futile
5,1,0,1,0,irregular verbs are the hardest to learn
6,1,0,1,0,shivering is one way to keep warm
7,1,0,1,0,the back yard of our house
8,1,0,1,0,i just cannot figure this out
9,1,0,1,0,these cookies are so amazing
10,1,0,1,0,correct your diction immediately
11,2,1,1,0,elephants are afraid of mice
12,2,1,1,0,drugs should be avoided
13,2,1,1,0,go out for some pizza and beer
14,2,1,1,0,want to join us for lunch
15,2,1,1,0,life is but a dream
16,2,1,1,0,the picket line gives me the chills
17,2,1,1,0,very reluctant to enter
18,2,1,1,0,machinery is too complicated
19,2,1,1,0,this system of taxation
20,2,1,1,0,protect your environment
21,3,1,1,0,fall is my favorite season
22,3,1,1,0,the algorithm is too complicated
23,3,1,1,0,join us on the patio
24,3,1,1,0,my preferred treat is chocolate
25,3,1,1,0,the cotton is high
26,3,1,1,0,the store will close at ten
27,3,1,1,0,i want to hold your hand
28,3,1,1,0,freud wrote of the ego
29,3,1,1,0,user friendly interface
30,3,1,1,0,circumstances are unacceptable
31,4,0,1,50,i listen to the tape every day
32,4,0,1,50,the living is easy
33,4,0,1,50,all good boys deserve fudge
34,4,0,1,50,dinosaurs have been extinct for ages
35,4,0,1,50,mom made her a turtleneck
36,4,0,1,50,we accept personal checks
37,4,0,1,50,the biggest hamburger i have ever seen
38,4,0,1,50,two or three cups of coffee
39,4,0,1,50,a security force of eight thousand
40,4,0,1,50,do you prefer a window seat
41,5,1,1,50,travel at the speed of light
42,5,1,1,50,the largest of the five oceans
43,5,1,1,50,take a coffee break
44,5,1,1,50,i am allergic to bees and peanuts
45,5,1,1,50,get aboard the ship is leaving
46,5,1,1,50,never too rich and never too thin
47,5,1,1,50,you must make an appointment
48,5,1,1,50,the cat has a pleasant temperament
49,5,1,1,50,the dog buried the bone
50,5,1,1,50,a psychiatrist will help you
51,6,1,1,50,the opposing team is over there
52,6,1,1,50,it is very windy today
53,6,1,1,50,the capitol of our nation
54,6,1,1,50,the protesters blocked all traffic
55,6,1,1,50,that land is owned by the government
56,6,1,1,50,he is just like everyone else
57,6,1,1,50,consequences of a wrong turn
58,6,1,1,50,the gun discharged by accident
59,6,1,1,50,sing the gospel and the blues
60,6,1,1,50,yes you are very smart
61,7,0,1,25,the accident scene is a shrine for fans
62,7,0,1,25,the cream rises to the top
63,7,0,1,25,that is a very odd question
64,7,0,1,25,you have my sympathy
65,7,0,1,25,in sharp contrast to your words
66,7,0,1,25,please follow the guidelines
67,7,0,1,25,taking the train is usually faster
68,7,0,1,25,he was wearing a sweatshirt
69,7,0,1,25,canada has three territories
70,7,0,1,25,starlight and dewdrop
71,8,1,1,25,i spilled coffee on the carpet
72,8,1,1,25,he is still on our team
73,8,1,1,25,an inefficient way to heat a house
74,8,1,1,25,he is shouting loudly
75,8,1,1,25,chemical spill took forever
76,8,1,1,25,the registration period is over
77,8,1,1,25,a dog is the best friend of a man
78,8,1,1,25,please try to be home before midnight
79,8,1,1,25,the fourth edition was better
80,8,1,1,25,this watch is too expensive
81,9,1,1,25,they love to yap about nothing
82,9,1,1,25,universities are too expensive
83,9,1,1,25,but the levee was dry
84,9,1,1,25,my favorite subject is psychology
85,9,1,1,25,he played a hero in that movie
86,9,1,1,25,these barracks are big enough
87,9,1,1,25,important news always seems to be late
88,9,1,1,25,express delivery is very fast
89,9,1,1,25,destruction of the rain forest
90,9,1,1,25,communicate through email
91,10,0,1,100,file all complaints in writing
92,10,0,1,100,burglars never leave their business card
93,10,0,1,100,the kids are very excited
94,10,0,1,100,public transit is much faster
95,10,0,1,100,for murder you get a long prison sentence
96,10,0,1,100,the power of denial
97,10,0,1,100,you should visit a doctor
98,10,0,1,100,i am wearing a tie and a jacket
99,10,0,1,100,quit while you are ahead
100,10,0,1,100,time to go shopping
101,11,1,1,100,safe to walk the streets in the evening
102,11,1,1,100,earthquakes are predictable
103,11,1,1,100,apartments are too expensive
104,11,1,1,100,flashing red light means stop
105,11,1,1,100,peek out the window
106,11,1,1,100,a much higher risk of getting cancer
107,11,1,1,100,do not walk too quickly
108,11,1,1,100,elections bring out the best
109,11,1,1,100,my dog sheds his hair
110,11,1,1,100,did you have a good time
111,12,1,1,100,the aspirations of a nation
112,12,1,1,100,important for political parties
113,12,1,1,100,peering through a small hole
114,12,1,1,100,saving that child was a heroic effort
115,12,1,1,100,i can play much better now
116,12,1,1,100,the winner of the race
117,12,1,1,100,we are having spaghetti
118,12,1,1,100,pumping brakes when roads are slippery
119,12,1,1,100,that is a very nasty cut
120,12,1,1,100,a quarter of a century
121,13,0,1,75,six daughters and seven sons
122,13,0,1,75,meet tomorrow in the lavatory
123,13,0,1,75,just in time for the party
124,13,0,1,75,our life expectancy has increased
125,13,0,1,75,a good response to the question
126,13,0,1,75,the fire raged for an entire month
127,13,0,1,75,is there any indication of this
128,13,0,1,75,we went grocery shopping
129,13,0,1,75,players must know all the rules
130,13,0,1,75,a good stimulus deserves a good response
131,14,1,1,75,find a nearby parking spot
132,14,1,1,75,everybody loses in custody battles
133,14,1,1,75,the trains are always late
134,14,1,1,75,the treasurer must balance her books
135,14,1,1,75,the world is a stage
136,14,1,1,75,mary had a little lamb
137,14,1,1,75,sad to hear that news
138,14,1,1,75,try to enjoy your maternity leave
139,14,1,1,75,gun powder must be handled with care
140,14,1,1,75,play it again sam
141,15,1,1,75,they might find your comment offensive
142,15,1,1,75,always cover all the bases
143,15,1,1,75,dormitory doors are locked at midnight
144,15,1,1,75,she wears too much makeup
145,15,1,1,75,valium in the economy size
146,15,1,1,75,the most beautiful sunset
147,15,1,1,75,insurance is important for bad drivers
148,15,1,1,75,sign the withdrawal slip
149,15,1,1,75,the generation gap gets wider
150,15,1,1,75,no kissing in the library
