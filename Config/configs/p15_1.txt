1,1,0,0,25,we missed your birthday
2,1,0,0,25,the four seasons will come
3,1,0,0,25,a good stimulus deserves a good response
4,1,0,0,25,parking lot is full of trucks
5,1,0,0,25,all work and no play
6,1,0,0,25,are you talking to me
7,1,0,0,25,the fire blazed all weekend
8,1,0,0,25,stability of the nation
9,1,0,0,25,with each step forward
10,1,0,0,25,knee bone is connected to the thigh bone
11,2,1,0,25,they might find your comment offensive
12,2,1,0,25,the voters turfed him out
13,2,1,0,25,please keep this confidential
14,2,1,0,25,bank transaction was not registered
15,2,1,0,25,i spilled coffee on the carpet
16,2,1,0,25,we went grocery shopping
17,2,1,0,25,jumping right out of the water
18,2,1,0,25,insurance is important for bad drivers
19,2,1,0,25,on the way to the cottage
20,2,1,0,25,universities are too expensive
21,3,1,0,25,our life expectancy has increased
22,3,1,0,25,the dog buried the bone
23,3,1,0,25,experience is hard to come by
24,3,1,0,25,the imagination of the nation
25,3,1,0,25,salesmen must make their monthly quota
26,3,1,0,25,my dog sheds his hair
27,3,1,0,25,granite is the hardest of all rocks
28,3,1,0,25,watch out for low flying objects
29,3,1,0,25,one of the poorest nations
30,3,1,0,25,my bare face in the wind
31,4,0,0,50,i hate baking pies
32,4,0,0,50,a psychiatrist will help you
33,4,0,0,50,we run the risk of failure
34,4,0,0,50,the library is closed today
35,4,0,0,50,careless driving results in a fine
36,4,0,0,50,nothing finer than discovering a treasure
37,4,0,0,50,she wears too much makeup
38,4,0,0,50,do you like to go camping
39,4,0,0,50,machinery is too complicated
40,4,0,0,50,prayer in schools offends some
41,5,1,0,50,relations are very strained
42,5,1,0,50,a touchdown in the last minute
43,5,1,0,50,i will meet you at noon
44,5,1,0,50,a glance in the right direction
45,5,1,0,50,the assignment is due today
46,5,1,0,50,what goes up must come down
47,5,1,0,50,the ventilation system is broken
48,5,1,0,50,please try to be home before midnight
49,5,1,0,50,rapidly running short on words
50,5,1,0,50,the union will go on strike
51,6,1,0,50,in sharp contrast to your words
52,6,1,0,50,the treasury department is broke
53,6,1,0,50,quick there is someone knocking
54,6,1,0,50,parking tickets can be challenged
55,6,1,0,50,be discreet about your meeting
56,6,1,0,50,there will be some fog tonight
57,6,1,0,50,movie about a nutty professor
58,6,1,0,50,correct your diction immediately
59,6,1,0,50,that agreement is rife with problems
60,6,1,0,50,they love to yap about nothing
61,7,0,0,0,elephants are afraid of mice
62,7,0,0,0,this phenomenon will never occur
63,7,0,0,0,bad for the environment
64,7,0,0,0,the plug does not fit the socket
65,7,0,0,0,flashing red light means stop
66,7,0,0,0,fish are jumping
67,7,0,0,0,never too rich and never too thin
68,7,0,0,0,get aboard the ship is leaving
69,7,0,0,0,are you sure you want this
70,7,0,0,0,you must make an appointment
71,8,1,0,0,my favorite subject is psychology
72,8,1,0,0,rain rain go away
73,8,1,0,0,it looks like a shack
74,8,1,0,0,user friendly interface
75,8,1,0,0,february has an extra day
76,8,1,0,0,good jobs for those with education
77,8,1,0,0,the sun rises in the east
78,8,1,0,0,traveling to conferences is fun
79,8,1,0,0,thank you for your help
80,8,1,0,0,join us on the patio
81,9,1,0,0,the treasurer must balance her books
82,9,1,0,0,i like baroque and classical music
83,9,1,0,0,do not walk too quickly
84,9,1,0,0,seasoned golfers love the game
85,9,1,0,0,we must redouble our efforts
86,9,1,0,0,i can see the rings on saturn
87,9,1,0,0,i am wearing a tie and a jacket
88,9,1,0,0,the dog will bite you
89,9,1,0,0,you have my sympathy
90,9,1,0,0,acutely aware of his good looks
91,10,0,0,75,can i skate with my sister today
92,10,0,0,75,motivational seminars make me sick
93,10,0,0,75,elections bring out the best
94,10,0,0,75,love means many things
95,10,0,0,75,the store will close at ten
96,10,0,0,75,the early bird gets the worm
97,10,0,0,75,healthy food is good for you
98,10,0,0,75,gun powder must be handled with care
99,10,0,0,75,travel at the speed of light
100,10,0,0,75,this watch is too expensive
101,11,1,0,75,suburbs are sprawling everywhere
102,11,1,0,75,universally understood to be wrong
103,11,1,0,75,i watched blazing saddles
104,11,1,0,75,gas bills are sent monthly
105,11,1,0,75,learn to walk before you run
106,11,1,0,75,a duck quacks to ask for food
107,11,1,0,75,coalition governments never work
108,11,1,0,75,a much higher risk of getting cancer
109,11,1,0,75,sharp cheese keeps the mind sharp
110,11,1,0,75,a tumor is okay provided it is benign
111,12,1,0,75,physics and chemistry are hard
112,12,1,0,75,the largest of the five oceans
113,12,1,0,75,this system of taxation
114,12,1,0,75,sign the withdrawal slip
115,12,1,0,75,burglars never leave their business card
116,12,1,0,75,protect your environment
117,12,1,0,75,every apple from every tree
118,12,1,0,75,this library has many books
119,12,1,0,75,if at first you do not succeed
120,12,1,0,75,information super highway
121,13,0,0,100,your etiquette needs some work
122,13,0,0,100,exceed the maximum speed limit
123,13,0,0,100,he played a hero in that movie
124,13,0,0,100,the acceptance speech was boring
125,13,0,0,100,superman never wore a mask
126,13,0,0,100,what a lovely red jacket
127,13,0,0,100,do not worry about this
128,13,0,0,100,longer than a football field
129,13,0,0,100,construction makes traveling difficult
130,13,0,0,100,video camera with a zoom lens
131,14,1,0,100,that is a very odd question
132,14,1,0,100,hands on experience with a job
133,14,1,0,100,reading week is just about here
134,14,1,0,100,the water was monitored daily
135,14,1,0,100,go out for some pizza and beer
136,14,1,0,100,sprawling subdivisions are bad
137,14,1,0,100,an injustice is committed every day
138,14,1,0,100,world population is growing
139,14,1,0,100,only an idiot would lie in court
140,14,1,0,100,call for more details
141,15,1,0,100,take a coffee break
142,15,1,0,100,mary had a little lamb
143,15,1,0,100,a fox is a very smart animal
144,15,1,0,100,a subject one can really enjoy
145,15,1,0,100,my favorite web browser
146,15,1,0,100,i like to play tennis
147,15,1,0,100,round robin scheduling
148,15,1,0,100,bring the offenders to justice
149,15,1,0,100,keep receipts for all your expenses
150,15,1,0,100,my watch fell in the water
