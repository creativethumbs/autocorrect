1,1,0,2,25,why do you ask silly questions
2,1,0,2,25,file all complaints in writing
3,1,0,2,25,this mission statement is baloney
4,1,0,2,25,victims deserve more redress
5,1,0,2,25,always cover all the bases
6,1,0,2,25,do not squander your time
7,1,0,2,25,make up a few more phrases
8,1,0,2,25,i can still feel your presence
9,1,0,2,25,good jobs for those with education
10,1,0,2,25,interesting observation was made
11,2,1,2,25,world population is growing
12,2,1,2,25,want to join us for lunch
13,2,1,2,25,a thoroughly disgusting thing to say
14,2,1,2,25,there will be some fog tonight
15,2,1,2,25,buckle up for safety
16,2,1,2,25,one hour is allotted for questions
17,2,1,2,25,the treasury department is broke
18,2,1,2,25,traveling to conferences is fun
19,2,1,2,25,neither a borrower nor a lender be
20,2,1,2,25,the insulation is not working
21,3,1,2,25,exceed the maximum speed limit
22,3,1,2,25,construction makes traveling difficult
23,3,1,2,25,dinosaurs have been extinct for ages
24,3,1,2,25,beware the ides of march
25,3,1,2,25,pumping brakes when roads are slippery
26,3,1,2,25,the plug does not fit the socket
27,3,1,2,25,i skimmed through your proposal
28,3,1,2,25,the largest of the five oceans
29,3,1,2,25,a yard is almost as long as a meter
30,3,1,2,25,microscopes make small things look big
31,4,0,2,75,he watched in astonishment
32,4,0,2,75,yes you are very smart
33,4,0,2,75,my bare face in the wind
34,4,0,2,75,please keep this confidential
35,4,0,2,75,the bathroom is good for reading
36,4,0,2,75,superman never wore a mask
37,4,0,2,75,in sharp contrast to your words
38,4,0,2,75,i can see the rings on saturn
39,4,0,2,75,i like baroque and classical music
40,4,0,2,75,i hate baking pies
41,5,1,2,75,handicapped persons need consideration
42,5,1,2,75,i will meet you at noon
43,5,1,2,75,try to enjoy your maternity leave
44,5,1,2,75,hair gel is very greasy
45,5,1,2,75,on the way to the cottage
46,5,1,2,75,can i skate with my sister today
47,5,1,2,75,a psychiatrist will help you
48,5,1,2,75,not quite so smart as you think
49,5,1,2,75,the objective of the exercise
50,5,1,2,75,that is a very odd question
51,6,1,2,75,circumstances are unacceptable
52,6,1,2,75,stay away from strangers
53,6,1,2,75,seasoned golfers love the game
54,6,1,2,75,safe to walk the streets in the evening
55,6,1,2,75,peering through a small hole
56,6,1,2,75,the power of denial
57,6,1,2,75,love means many things
58,6,1,2,75,rapidly running short on words
59,6,1,2,75,chemical spill took forever
60,6,1,2,75,a feeling of complete exasperation
61,7,0,2,100,gas bills are sent monthly
62,7,0,2,100,every saturday he folds the laundry
63,7,0,2,100,i agree with you
64,7,0,2,100,labor unions know how to organize
65,7,0,2,100,prevailing wind from the east
66,7,0,2,100,batman wears a cape
67,7,0,2,100,what a monkey sees a monkey will do
68,7,0,2,100,the minimum amount of time
69,7,0,2,100,most judges are very honest
70,7,0,2,100,the voters turfed him out
71,8,1,2,100,the location of the crime
72,8,1,2,100,travel at the speed of light
73,8,1,2,100,the daring young man
74,8,1,2,100,mary had a little lamb
75,8,1,2,100,rent is paid at the beginning of the month
76,8,1,2,100,zero in on the facts
77,8,1,2,100,parking lot is full of trucks
78,8,1,2,100,wishful thinking is fine
79,8,1,2,100,this equation is too complicated
80,8,1,2,100,my favorite place to visit
81,9,1,2,100,please take a bath this month
82,9,1,2,100,life is but a dream
83,9,1,2,100,correct your diction immediately
84,9,1,2,100,do not drink too much
85,9,1,2,100,insurance is important for bad drivers
86,9,1,2,100,i spilled coffee on the carpet
87,9,1,2,100,i am wearing a tie and a jacket
88,9,1,2,100,this leather jacket is too warm
89,9,1,2,100,the dreamers of dreams
90,9,1,2,100,the cream rises to the top
91,10,0,2,50,user friendly interface
92,10,0,2,50,every apple from every tree
93,10,0,2,50,my favorite web browser
94,10,0,2,50,he called seven times
95,10,0,2,50,we park in driveways
96,10,0,2,50,the picket line gives me the chills
97,10,0,2,50,a good stimulus deserves a good response
98,10,0,2,50,you should visit a doctor
99,10,0,2,50,he is shouting loudly
100,10,0,2,50,get your priorities in order
101,11,1,2,50,double double toil and trouble
102,11,1,2,50,a problem with the engine
103,11,1,2,50,saving that child was a heroic effort
104,11,1,2,50,are you sure you want this
105,11,1,2,50,the store will close at ten
106,11,1,2,50,the proprietor was unavailable
107,11,1,2,50,the opposing team is over there
108,11,1,2,50,one never takes too many precautions
109,11,1,2,50,the world is a stage
110,11,1,2,50,my dog sheds his hair
111,12,1,2,50,the dow jones index has risen
112,12,1,2,50,the price of gas is high
113,12,1,2,50,shall we play a round of cards
114,12,1,2,50,pay off a mortgage for a house
115,12,1,2,50,mystery of the lost lagoon
116,12,1,2,50,healthy food is good for you
117,12,1,2,50,the chamber makes important decisions
118,12,1,2,50,the dog will bite you
119,12,1,2,50,historic meeting without a result
120,12,1,2,50,the union will go on strike
121,13,0,2,0,the biggest hamburger i have ever seen
122,13,0,2,0,sing the gospel and the blues
123,13,0,2,0,only an idiot would lie in court
124,13,0,2,0,do not say anything
125,13,0,2,0,the trains are always late
126,13,0,2,0,we missed your birthday
127,13,0,2,0,everybody loses in custody battles
128,13,0,2,0,the sun rises in the east
129,13,0,2,0,parking tickets can be challenged
130,13,0,2,0,this library has many books
131,14,1,2,0,motivational seminars make me sick
132,14,1,2,0,i am going to a music lesson
133,14,1,2,0,the capitol of our nation
134,14,1,2,0,information super highway
135,14,1,2,0,taking the train is usually faster
136,14,1,2,0,drugs should be avoided
137,14,1,2,0,the registration period is over
138,14,1,2,0,get rid of that immediately
139,14,1,2,0,mom made her a turtleneck
140,14,1,2,0,the fax machine is broken
141,15,1,2,0,rectangular objects have four sides
142,15,1,2,0,do not drink the water
143,15,1,2,0,the force is with you
144,15,1,2,0,important news always seems to be late
145,15,1,2,0,we are subjects and must obey
146,15,1,2,0,look in the syllabus for the course
147,15,1,2,0,it is very windy today
148,15,1,2,0,the collapse of the roman empire
149,15,1,2,0,you are a wonderful example
150,15,1,2,0,public transit is much faster
