1,1,0,0,50,the pen is mightier than the sword
2,1,0,0,50,a coupon for a free sample
3,1,0,0,50,bring the offenders to justice
4,1,0,0,50,important for political parties
5,1,0,0,50,my preferred treat is chocolate
6,1,0,0,50,i can play much better now
7,1,0,0,50,the price of gas is high
8,1,0,0,50,the bus was very crowded
9,1,0,0,50,never too rich and never too thin
10,1,0,0,50,beware the ides of march
11,2,1,0,50,they watched the entire movie
12,2,1,0,50,where can my little dog be
13,2,1,0,50,spill coffee on the carpet
14,2,1,0,50,did you have a good time
15,2,1,0,50,drugs should be avoided
16,2,1,0,50,the gun discharged by accident
17,2,1,0,50,can we play cards tonight
18,2,1,0,50,starlight and dewdrop
19,2,1,0,50,please take a bath this month
20,2,1,0,50,are you sure you want this
21,3,1,0,50,an occasional taste of chocolate
22,3,1,0,50,be home before midnight
23,3,1,0,50,one of the poorest nations
24,3,1,0,50,popularity is desired by all
25,3,1,0,50,rejection letters are discouraging
26,3,1,0,50,i am allergic to bees and peanuts
27,3,1,0,50,be discreet about your meeting
28,3,1,0,50,a fox is a very smart animal
29,3,1,0,50,arguing with the boss is futile
30,3,1,0,50,earthquakes are predictable
31,4,0,0,75,an injustice is committed every day
32,4,0,0,75,well connected with people
33,4,0,0,75,the elevator door appears to be stuck
34,4,0,0,75,the world is a stage
35,4,0,0,75,the opposing team is over there
36,4,0,0,75,we have enough witnesses
37,4,0,0,75,sign the withdrawal slip
38,4,0,0,75,every saturday he folds the laundry
39,4,0,0,75,construction makes traveling difficult
40,4,0,0,75,make up a few more phrases
41,5,1,0,75,burglars never leave their business card
42,5,1,0,75,historic meeting without a result
43,5,1,0,75,faster than a speeding bullet
44,5,1,0,75,we run the risk of failure
45,5,1,0,75,do a good deed to someone
46,5,1,0,75,completely sold out of that
47,5,1,0,75,this camera takes nice photographs
48,5,1,0,75,our fax number has changed
49,5,1,0,75,mary had a little lamb
50,5,1,0,75,please follow the guidelines
51,6,1,0,75,he called seven times
52,6,1,0,75,my mother makes good cookies
53,6,1,0,75,the dog buried the bone
54,6,1,0,75,a thoroughly disgusting thing to say
55,6,1,0,75,chemical spill took forever
56,6,1,0,75,the ropes of a new organization
57,6,1,0,75,valium in the economy size
58,6,1,0,75,granite is the hardest of all rocks
59,6,1,0,75,our housekeeper does a thorough job
60,6,1,0,75,the dow jones index has risen
61,7,0,0,25,the accident scene is a shrine for fans
62,7,0,0,25,on the way to the cottage
63,7,0,0,25,the ventilation system is broken
64,7,0,0,25,there will be some fog tonight
65,7,0,0,25,dolphins leap high out of the water
66,7,0,0,25,fish are jumping
67,7,0,0,25,presidents drive expensive cars
68,7,0,0,25,vanilla flavored ice cream
69,7,0,0,25,why do you ask silly questions
70,7,0,0,25,an excellent way to communicate
71,8,1,0,25,a lot of chlorine in the water
72,8,1,0,25,did you see that spectacular explosion
73,8,1,0,25,coming up with killer sound bites
74,8,1,0,25,want to join us for lunch
75,8,1,0,25,that is a very odd question
76,8,1,0,25,one hour is allotted for questions
77,8,1,0,25,what goes up must come down
78,8,1,0,25,the chancellor was very boring
79,8,1,0,25,important news always seems to be late
80,8,1,0,25,sent this by registered mail
81,9,1,0,25,question that must be answered
82,9,1,0,25,prayer in schools offends some
83,9,1,0,25,do not feel too bad about it
84,9,1,0,25,the registration period is over
85,9,1,0,25,a rattle snake is very poisonous
86,9,1,0,25,would you like to come to my house
87,9,1,0,25,learn to walk before you run
88,9,1,0,25,hands on experience with a job
89,9,1,0,25,stay away from strangers
90,9,1,0,25,it looks like a shack
91,10,0,0,0,the minimum amount of time
92,10,0,0,0,sit at the front of the bus
93,10,0,0,0,zero in on the facts
94,10,0,0,0,traveling requires a lot of fuel
95,10,0,0,0,this is a non profit organization
96,10,0,0,0,a touchdown in the last minute
97,10,0,0,0,seasoned golfers love the game
98,10,0,0,0,only an idiot would lie in court
99,10,0,0,0,communicate through email
100,10,0,0,0,an inefficient way to heat a house
101,11,1,0,0,it should be sunny tomorrow
102,11,1,0,0,join us on the patio
103,11,1,0,0,these barracks are big enough
104,11,1,0,0,acutely aware of his good looks
105,11,1,0,0,your etiquette needs some work
106,11,1,0,0,public transit is much faster
107,11,1,0,0,are you talking to me
108,11,1,0,0,your presentation was inspiring
109,11,1,0,0,do not drink too much
110,11,1,0,0,this leather jacket is too warm
111,12,1,0,0,mom made her a turtleneck
112,12,1,0,0,the daring young man
113,12,1,0,0,the sum of the parts
114,12,1,0,0,a tumor is okay provided it is benign
115,12,1,0,0,they might find your comment offensive
116,12,1,0,0,fine but only in moderation
117,12,1,0,0,that land is owned by the government
118,12,1,0,0,people blow their own horn a lot
119,12,1,0,0,superman never wore a mask
120,12,1,0,0,vote according to your conscience
121,13,0,0,100,you are not a jedi yet
122,13,0,0,100,never mix religion and politics
123,13,0,0,100,six daughters and seven sons
124,13,0,0,100,i just cannot figure this out
125,13,0,0,100,you should visit a doctor
126,13,0,0,100,a feeling of complete exasperation
127,13,0,0,100,staying up all night is a bad idea
128,13,0,0,100,the bathroom is good for reading
129,13,0,0,100,the collapse of the roman empire
130,13,0,0,100,the protesters blocked all traffic
131,14,1,0,100,limited warranty of two years
132,14,1,0,100,just what the doctor ordered
133,14,1,0,100,double double toil and trouble
134,14,1,0,100,a yard is almost as long as a meter
135,14,1,0,100,please provide your date of birth
136,14,1,0,100,fall is my favorite season
137,14,1,0,100,yes you are very smart
138,14,1,0,100,traveling to conferences is fun
139,14,1,0,100,space is a high priority
140,14,1,0,100,beautiful paintings in the gallery
141,15,1,0,100,this phenomenon will never occur
142,15,1,0,100,but the levee was dry
143,15,1,0,100,three two one zero blast off
144,15,1,0,100,information super highway
145,15,1,0,100,my favorite place to visit
146,15,1,0,100,this library has many books
147,15,1,0,100,the stock exchange dipped
148,15,1,0,100,take a coffee break
149,15,1,0,100,we better investigate this
150,15,1,0,100,a subject one can really enjoy
