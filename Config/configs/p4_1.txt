1,1,0,1,50,correct your diction immediately
2,1,0,1,50,this leather jacket is too warm
3,1,0,1,50,the stock exchange dipped
4,1,0,1,50,my mother makes good cookies
5,1,0,1,50,it is difficult to concentrate
6,1,0,1,50,i watched blazing saddles
7,1,0,1,50,protect your environment
8,1,0,1,50,universities are too expensive
9,1,0,1,50,win first prize in the contest
10,1,0,1,50,but the levee was dry
11,2,1,1,50,beautiful paintings in the gallery
12,2,1,1,50,the bus was very crowded
13,2,1,1,50,i can still feel your presence
14,2,1,1,50,please keep this confidential
15,2,1,1,50,stay away from strangers
16,2,1,1,50,superman never wore a mask
17,2,1,1,50,the world is a stage
18,2,1,1,50,he is just like everyone else
19,2,1,1,50,every apple from every tree
20,2,1,1,50,questioning the wisdom of the courts
21,3,1,1,50,the assignment is due today
22,3,1,1,50,my bike has a flat tire
23,3,1,1,50,we drive on parkways
24,3,1,1,50,this phenomenon will never occur
25,3,1,1,50,canada has ten provinces
26,3,1,1,50,freud wrote of the ego
27,3,1,1,50,house with new electrical panel
28,3,1,1,50,it is very windy today
29,3,1,1,50,knee bone is connected to the thigh bone
30,3,1,1,50,reading week is just about here
31,4,0,1,100,shivering is one way to keep warm
32,4,0,1,100,an airport is a very busy place
33,4,0,1,100,i am wearing a tie and a jacket
34,4,0,1,100,he called seven times
35,4,0,1,100,presidents drive expensive cars
36,4,0,1,100,what you see is what you get
37,4,0,1,100,that agreement is rife with problems
38,4,0,1,100,get rid of that immediately
39,4,0,1,100,this is too much to handle
40,4,0,1,100,a feeling of complete exasperation
41,5,1,1,100,dinosaurs have been extinct for ages
42,5,1,1,100,double double toil and trouble
43,5,1,1,100,you want to eat your cake
44,5,1,1,100,spill coffee on the carpet
45,5,1,1,100,all work and no play
46,5,1,1,100,are you talking to me
47,5,1,1,100,be discreet about your meeting
48,5,1,1,100,you must make an appointment
49,5,1,1,100,the fire blazed all weekend
50,5,1,1,100,a tumor is okay provided it is benign
51,6,1,1,100,go out for some pizza and beer
52,6,1,1,100,chemical spill took forever
53,6,1,1,100,we run the risk of failure
54,6,1,1,100,head shoulders knees and toes
55,6,1,1,100,you have my sympathy
56,6,1,1,100,the treasurer must balance her books
57,6,1,1,100,take a coffee break
58,6,1,1,100,where did you get that tie
59,6,1,1,100,popularity is desired by all
60,6,1,1,100,the imagination of the nation
61,7,0,1,25,he cooled off after she left
62,7,0,1,25,consequences of a wrong turn
63,7,0,1,25,the food at this restaurant
64,7,0,1,25,parking tickets can be challenged
65,7,0,1,25,want to join us for lunch
66,7,0,1,25,that land is owned by the government
67,7,0,1,25,he underwent triple bypass surgery
68,7,0,1,25,the back yard of our house
69,7,0,1,25,file all complaints in writing
70,7,0,1,25,her majesty visited our country
71,8,1,1,25,limited warranty of two years
72,8,1,1,25,a rattle snake is very poisonous
73,8,1,1,25,space is a high priority
74,8,1,1,25,the protesters blocked all traffic
75,8,1,1,25,weeping willows are found near water
76,8,1,1,25,my preferred treat is chocolate
77,8,1,1,25,please provide your date of birth
78,8,1,1,25,valium in the economy size
79,8,1,1,25,in sharp contrast to your words
80,8,1,1,25,dormitory doors are locked at midnight
81,9,1,1,25,the library is closed today
82,9,1,1,25,the dow jones index has risen
83,9,1,1,25,teaching services will help
84,9,1,1,25,electric cars need big fuel cells
85,9,1,1,25,the capitol of our nation
86,9,1,1,25,nothing finer than discovering a treasure
87,9,1,1,25,universally understood to be wrong
88,9,1,1,25,my favorite place to visit
89,9,1,1,25,a security force of eight thousand
90,9,1,1,25,make up a few more phrases
91,10,0,1,0,question that must be answered
92,10,0,1,0,the force is with you
93,10,0,1,0,come and see our new car
94,10,0,1,0,i do not fully agree with you
95,10,0,1,0,the proprietor was unavailable
96,10,0,1,0,listen to five hours of opera
97,10,0,1,0,that is very unfortunate
98,10,0,1,0,the water was monitored daily
99,10,0,1,0,the location of the crime
100,10,0,1,0,good at addition and subtraction
101,11,1,1,0,olympic athletes use drugs
102,11,1,1,0,taking the train is usually faster
103,11,1,1,0,rectangular objects have four sides
104,11,1,1,0,these cookies are so amazing
105,11,1,1,0,the second largest country
106,11,1,1,0,arguing with the boss is futile
107,11,1,1,0,a duck quacks to ask for food
108,11,1,1,0,tickets are very expensive
109,11,1,1,0,burglars never leave their business card
110,11,1,1,0,the cream rises to the top
111,12,1,1,0,a psychiatrist will help you
112,12,1,1,0,two or three cups of coffee
113,12,1,1,0,i do not care if you do that
114,12,1,1,0,a fox is a very smart animal
115,12,1,1,0,the chancellor was very boring
116,12,1,1,0,i like to play tennis
117,12,1,1,0,gun powder must be handled with care
118,12,1,1,0,round robin scheduling
119,12,1,1,0,players must know all the rules
120,12,1,1,0,this mission statement is baloney
121,13,0,1,75,a steep learning curve in riding a unicycle
122,13,0,1,75,the early bird gets the worm
123,13,0,1,75,rejection letters are discouraging
124,13,0,1,75,one hour is allotted for questions
125,13,0,1,75,motivational seminars make me sick
126,13,0,1,75,vanilla flavored ice cream
127,13,0,1,75,rent is paid at the beginning of the month
128,13,0,1,75,the four seasons will come
129,13,0,1,75,gamblers eventually lose their shirts
130,13,0,1,75,is there any indication of this
131,14,1,1,75,traveling requires a lot of fuel
132,14,1,1,75,my fingers are very cold
133,14,1,1,75,have a good weekend
134,14,1,1,75,it looks like a shack
135,14,1,1,75,the fourth edition was better
136,14,1,1,75,do not squander your time
137,14,1,1,75,dashing through the snow
138,14,1,1,75,parking lot is full of trucks
139,14,1,1,75,play it again sam
140,14,1,1,75,keep receipts for all your expenses
141,15,1,1,75,the chamber makes important decisions
142,15,1,1,75,thank you for your help
143,15,1,1,75,the acceptance speech was boring
144,15,1,1,75,this watch is too expensive
145,15,1,1,75,this is a non profit organization
146,15,1,1,75,great disturbance in the force
147,15,1,1,75,prayer in schools offends some
148,15,1,1,75,the algorithm is too complicated
149,15,1,1,75,soon we will return from the city
150,15,1,1,75,see you later alligator
