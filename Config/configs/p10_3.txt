1,1,0,2,50,my car always breaks in the winter
2,1,0,2,50,get aboard the ship is leaving
3,1,0,2,50,vote according to your conscience
4,1,0,2,50,the winner of the race
5,1,0,2,50,stability of the nation
6,1,0,2,50,good jobs for those with education
7,1,0,2,50,the collapse of the roman empire
8,1,0,2,50,file all complaints in writing
9,1,0,2,50,rent is paid at the beginning of the month
10,1,0,2,50,the opposing team is over there
11,2,1,2,50,the dow jones index has risen
12,2,1,2,50,i skimmed through your proposal
13,2,1,2,50,breathing is difficult
14,2,1,2,50,electric cars need big fuel cells
15,2,1,2,50,great disturbance in the force
16,2,1,2,50,this watch is too expensive
17,2,1,2,50,a little encouragement is needed
18,2,1,2,50,everyone wants to win the lottery
19,2,1,2,50,the plug does not fit the socket
20,2,1,2,50,i want to hold your hand
21,3,1,2,50,would you like to come to my house
22,3,1,2,50,can we play cards tonight
23,3,1,2,50,our life expectancy has increased
24,3,1,2,50,there will be some fog tonight
25,3,1,2,50,the accident scene is a shrine for fans
26,3,1,2,50,effort is what it will take
27,3,1,2,50,raindrops keep falling on my head
28,3,1,2,50,the laser printer is jammed
29,3,1,2,50,an offer you cannot refuse
30,3,1,2,50,the assignment is due today
31,4,0,2,100,this camera takes nice photographs
32,4,0,2,100,the bus was very crowded
33,4,0,2,100,completely sold out of that
34,4,0,2,100,valid until the end of the year
35,4,0,2,100,the generation gap gets wider
36,4,0,2,100,i watched blazing saddles
37,4,0,2,100,my mother makes good cookies
38,4,0,2,100,so you think you deserve a raise
39,4,0,2,100,the insulation is not working
40,4,0,2,100,an injustice is committed every day
41,5,1,2,100,a dog is the best friend of a man
42,5,1,2,100,i do not fully agree with you
43,5,1,2,100,gas bills are sent monthly
44,5,1,2,100,communicate through email
45,5,1,2,100,peering through a small hole
46,5,1,2,100,that sticker needs to be validated
47,5,1,2,100,beware the ides of march
48,5,1,2,100,the presidential suite is very busy
49,5,1,2,100,coming up with killer sound bites
50,5,1,2,100,elephants are afraid of mice
51,6,1,2,100,freud wrote of the ego
52,6,1,2,100,do you like to go camping
53,6,1,2,100,the fire raged for an entire month
54,6,1,2,100,this mission statement is baloney
55,6,1,2,100,please provide your date of birth
56,6,1,2,100,do a good deed to someone
57,6,1,2,100,universally understood to be wrong
58,6,1,2,100,your presentation was inspiring
59,6,1,2,100,labor unions know how to organize
60,6,1,2,100,my favorite place to visit
61,7,0,2,0,medieval times were very hard
62,7,0,2,0,machinery is too complicated
63,7,0,2,0,all good boys deserve fudge
64,7,0,2,0,on the way to the cottage
65,7,0,2,0,prescription drugs require a note
66,7,0,2,0,this library has many books
67,7,0,2,0,the four seasons will come
68,7,0,2,0,prepare for the exam in advance
69,7,0,2,0,where did you get such a silly idea
70,7,0,2,0,a good joke deserves a good laugh
71,8,1,2,0,suburbs are sprawling everywhere
72,8,1,2,0,exercise is good for the mind
73,8,1,2,0,there are winners and losers
74,8,1,2,0,he called seven times
75,8,1,2,0,the living is easy
76,8,1,2,0,the children are playing
77,8,1,2,0,hands on experience with a job
78,8,1,2,0,the ventilation system is broken
79,8,1,2,0,i just cannot figure this out
80,8,1,2,0,seasoned golfers love the game
81,9,1,2,0,prevailing wind from the east
82,9,1,2,0,fall is my favorite season
83,9,1,2,0,this is a very good idea
84,9,1,2,0,he is still on our team
85,9,1,2,0,you should visit a doctor
86,9,1,2,0,what a monkey sees a monkey will do
87,9,1,2,0,i am allergic to bees and peanuts
88,9,1,2,0,for murder you get a long prison sentence
89,9,1,2,0,it looks like a shack
90,9,1,2,0,that agreement is rife with problems
91,10,0,2,25,take a coffee break
92,10,0,2,25,i agree with you
93,10,0,2,25,the power of denial
94,10,0,2,25,the king sends you to the tower
95,10,0,2,25,my dog sheds his hair
96,10,0,2,25,the picket line gives me the chills
97,10,0,2,25,prayer in schools offends some
98,10,0,2,25,a duck quacks to ask for food
99,10,0,2,25,double double toil and trouble
100,10,0,2,25,see you later alligator
101,11,1,2,25,consequences of a wrong turn
102,11,1,2,25,if you come home late the doors are locked
103,11,1,2,25,we drive on parkways
104,11,1,2,25,drugs should be avoided
105,11,1,2,25,everybody loses in custody battles
106,11,1,2,25,sign the withdrawal slip
107,11,1,2,25,the cotton is high
108,11,1,2,25,the elevator door appears to be stuck
109,11,1,2,25,your etiquette needs some work
110,11,1,2,25,no more war no more bloodshed
111,12,1,2,25,be home before midnight
112,12,1,2,25,coalition governments never work
113,12,1,2,25,bank transaction was not registered
114,12,1,2,25,world population is growing
115,12,1,2,25,i spilled coffee on the carpet
116,12,1,2,25,just like it says on the canned good
117,12,1,2,25,the proprietor was unavailable
118,12,1,2,25,exceed the maximum speed limit
119,12,1,2,25,weeping willows are found near water
120,12,1,2,25,the water was monitored daily
121,13,0,2,75,careless driving results in a fine
122,13,0,2,75,traveling requires a lot of fuel
123,13,0,2,75,acutely aware of his good looks
124,13,0,2,75,tell a lie and your nose will grow
125,13,0,2,75,the first time he tried to swim
126,13,0,2,75,every saturday he folds the laundry
127,13,0,2,75,we dine out on the weekends
128,13,0,2,75,this system of taxation
129,13,0,2,75,apartments are too expensive
130,13,0,2,75,the imagination of the nation
131,14,1,2,75,vanilla flavored ice cream
132,14,1,2,75,watch out for low flying objects
133,14,1,2,75,what a lovely red jacket
134,14,1,2,75,her majesty visited our country
135,14,1,2,75,microscopes make small things look big
136,14,1,2,75,i like baroque and classical music
137,14,1,2,75,beautiful paintings in the gallery
138,14,1,2,75,where did i leave my glasses
139,14,1,2,75,the ropes of a new organization
140,14,1,2,75,zero in on the facts
141,15,1,2,75,gun powder must be handled with care
142,15,1,2,75,construction makes traveling difficult
143,15,1,2,75,salesmen must make their monthly quota
144,15,1,2,75,correct your diction immediately
145,15,1,2,75,do not drink the water
146,15,1,2,75,the dog will bite you
147,15,1,2,75,a good response to the question
148,15,1,2,75,space is a high priority
149,15,1,2,75,lydia wants to go home
150,15,1,2,75,the dreamers of dreams
