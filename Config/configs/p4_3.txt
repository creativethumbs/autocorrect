1,1,0,2,100,faster than a speeding bullet
2,1,0,2,100,do not walk too quickly
3,1,0,2,100,space is a high priority
4,1,0,2,100,the power of denial
5,1,0,2,100,join us on the patio
6,1,0,2,100,historic meeting without a result
7,1,0,2,100,pumping brakes when roads are slippery
8,1,0,2,100,pay off a mortgage for a house
9,1,0,2,100,rain rain go away
10,1,0,2,100,round robin scheduling
11,2,1,2,100,make my day you sucker
12,2,1,2,100,elephants are afraid of mice
13,2,1,2,100,they watched the entire movie
14,2,1,2,100,an airport is a very busy place
15,2,1,2,100,the quick brown fox jumped
16,2,1,2,100,interactions between men and women
17,2,1,2,100,get rid of that immediately
18,2,1,2,100,did you see that spectacular explosion
19,2,1,2,100,the early bird gets the worm
20,2,1,2,100,my favorite subject is psychology
21,3,1,2,100,fine but only in moderation
22,3,1,2,100,a subject one can really enjoy
23,3,1,2,100,companies announce a merger
24,3,1,2,100,give me one spoonful of coffee
25,3,1,2,100,parking lot is full of trucks
26,3,1,2,100,gun powder must be handled with care
27,3,1,2,100,we better investigate this
28,3,1,2,100,this watch is too expensive
29,3,1,2,100,prevailing wind from the east
30,3,1,2,100,we run the risk of failure
31,4,0,2,50,try to enjoy your maternity leave
32,4,0,2,50,a touchdown in the last minute
33,4,0,2,50,dinosaurs have been extinct for ages
34,4,0,2,50,toss the ball around
35,4,0,2,50,mom made her a turtleneck
36,4,0,2,50,you will lose your voice
37,4,0,2,50,quick there is someone knocking
38,4,0,2,50,we dine out on the weekends
39,4,0,2,50,a good response to the question
40,4,0,2,50,the cat has a pleasant temperament
41,5,1,2,50,vanilla flavored ice cream
42,5,1,2,50,your presentation was inspiring
43,5,1,2,50,he underwent triple bypass surgery
44,5,1,2,50,the capitol of our nation
45,5,1,2,50,the aspirations of a nation
46,5,1,2,50,a good joke deserves a good laugh
47,5,1,2,50,time to go shopping
48,5,1,2,50,all good boys deserve fudge
49,5,1,2,50,she wears too much makeup
50,5,1,2,50,i do not care if you do that
51,6,1,2,50,have a good weekend
52,6,1,2,50,canada has three territories
53,6,1,2,50,it should be sunny tomorrow
54,6,1,2,50,important news always seems to be late
55,6,1,2,50,get aboard the ship is leaving
56,6,1,2,50,the presidential suite is very busy
57,6,1,2,50,go out for some pizza and beer
58,6,1,2,50,my mother makes good cookies
59,6,1,2,50,our life expectancy has increased
60,6,1,2,50,labor unions know how to organize
61,7,0,2,25,valium in the economy size
62,7,0,2,25,the living is easy
63,7,0,2,25,saving that child was a heroic effort
64,7,0,2,25,exercise is good for the mind
65,7,0,2,25,just what the doctor ordered
66,7,0,2,25,nothing finer than discovering a treasure
67,7,0,2,25,work hard to reach the summit
68,7,0,2,25,a tumor is okay provided it is benign
69,7,0,2,25,physics and chemistry are hard
70,7,0,2,25,elections bring out the best
71,8,1,2,25,starlight and dewdrop
72,8,1,2,25,safe to walk the streets in the evening
73,8,1,2,25,experience is hard to come by
74,8,1,2,25,the facts get in the way
75,8,1,2,25,take a coffee break
76,8,1,2,25,universities are too expensive
77,8,1,2,25,sing the gospel and the blues
78,8,1,2,25,victims deserve more redress
79,8,1,2,25,learn to walk before you run
80,8,1,2,25,i watched blazing saddles
81,9,1,2,25,movie about a nutty professor
82,9,1,2,25,do you prefer a window seat
83,9,1,2,25,he was wearing a sweatshirt
84,9,1,2,25,microscopes make small things look big
85,9,1,2,25,there are winners and losers
86,9,1,2,25,shall we play a round of cards
87,9,1,2,25,these cookies are so amazing
88,9,1,2,25,why do you ask silly questions
89,9,1,2,25,he played a hero in that movie
90,9,1,2,25,shivering is one way to keep warm
91,10,0,2,0,my bare face in the wind
92,10,0,2,0,are you sure you want this
93,10,0,2,0,you are an ardent capitalist
94,10,0,2,0,is there any indication of this
95,10,0,2,0,handicapped persons need consideration
96,10,0,2,0,this system of taxation
97,10,0,2,0,if diplomacy does not work
98,10,0,2,0,the assault took six months
99,10,0,2,0,get your priorities in order
100,10,0,2,0,my preferred treat is chocolate
101,11,1,2,0,you must be getting old
102,11,1,2,0,one heck of a question
103,11,1,2,0,the library is closed today
104,11,1,2,0,drugs should be avoided
105,11,1,2,0,he called seven times
106,11,1,2,0,my favorite place to visit
107,11,1,2,0,these barracks are big enough
108,11,1,2,0,stiff penalty for staying out late
109,11,1,2,0,that is very unfortunate
110,11,1,2,0,public transit is much faster
111,12,1,2,0,a psychiatrist will help you
112,12,1,2,0,please take a bath this month
113,12,1,2,0,flashing red light means stop
114,12,1,2,0,careless driving results in a fine
115,12,1,2,0,do not lie in court or else
116,12,1,2,0,the collapse of the roman empire
117,12,1,2,0,coming up with killer sound bites
118,12,1,2,0,the kids are very excited
119,12,1,2,0,love means many things
120,12,1,2,0,the children are playing
121,13,0,2,75,that land is owned by the government
122,13,0,2,75,a lot of chlorine in the water
123,13,0,2,75,hands on experience with a job
124,13,0,2,75,the fourth edition was better
125,13,0,2,75,i like baroque and classical music
126,13,0,2,75,rent is paid at the beginning of the month
127,13,0,2,75,every saturday he folds the laundry
128,13,0,2,75,a coupon for a free sample
129,13,0,2,75,house with new electrical panel
130,13,0,2,75,i agree with you
131,14,1,2,75,i will put on my glasses
132,14,1,2,75,a glance in the right direction
133,14,1,2,75,six daughters and seven sons
134,14,1,2,75,we have enough witnesses
135,14,1,2,75,prescription drugs require a note
136,14,1,2,75,the opposing team is over there
137,14,1,2,75,prepare for the exam in advance
138,14,1,2,75,the chamber makes important decisions
139,14,1,2,75,peering through a small hole
140,14,1,2,75,our fax number has changed
141,15,1,2,75,dashing through the snow
142,15,1,2,75,one of the poorest nations
143,15,1,2,75,mystery of the lost lagoon
144,15,1,2,75,the bathroom is good for reading
145,15,1,2,75,just in time for the party
146,15,1,2,75,you should visit a doctor
147,15,1,2,75,mary had a little lamb
148,15,1,2,75,my dog sheds his hair
149,15,1,2,75,we went grocery shopping
150,15,1,2,75,motivational seminars make me sick
