import random,os,re
from itertools import cycle

debug = False

txtfolder = '/Users/tichaseth/Documents/MSCS F17/Autocorrection/autocorrect/Analysis/analysis'
outfile = open("analysis.csv", 'w')

outfile.write("Participant Number,Condition,Accuracy,Testing?,WPM,Efficiency,UncErrRate,CorErrRate,AutoCorErrRate,ManCorErrRate,TotErrRate\n")

# dictionaries that map sessions to conditions
# 0 = autocorrect only, 1 = listcorrect only, 2 = both
odddict = {"1":"0", "2":"1", "3":"2"}  # odd participant ids
evendict = {"1":"1", "2":"0", "3":"2"} # even participant ids

for filename in os.listdir(txtfolder):
    if filename.endswith(".txt"):
        # participant number
        filename = filename.strip()

        code = filename.split('_')
        pnum = code[0][1:]
        cond = code[1] # initialize to session first

        if int(pnum)%2 == 1:
                cond = odddict[cond]
        else:
                cond = evendict[cond]

        acc = code[2].split('.')[0]

        infile = os.path.join(txtfolder, filename)

        plsexit = False
        headers = []

        with open(infile) as fp:
                prevtask = 0

                for i,line in enumerate(fp):
                        line = line.strip()
                        if plsexit:
                                fp.close()
                                break

                        if i == 5:
                                headers = line.split(' ')
                        if 7 <= i:

                                if len(line) == 0:
                                        plsexit = True
                                        break

                                outfile.write(pnum + ',' + cond + ',' + acc + ',')
                                stats = line.split(' ')

                                if debug:
                                        print filename
                                        for j,elem in enumerate(headers):
                                                print elem + " : " + stats[j]
                                        print "\n"

                                tasknum = stats[0]
                                if int(tasknum) <= prevtask:
                                        print filename+" -- task: " + tasknum + ", prev task: " + str(prevtask)

                                prevtask = int(tasknum)
                                testing = stats[1]
                                wpm = stats[3]
                                effic = stats[23]
                                UncErrRate = stats[18]
                                CorErrRate = stats[19]
                                AutoCorErrRate = stats[20]
                                ManCorErrRate = stats[21]
                                TotErrRate = stats[22]

                                if cond == "0": # autocorrect only
                                        assert(CorErrRate == "0")
                                        assert(ManCorErrRate == "0")
                                if cond == "1": # listcorrect only
                                        assert(AutoCorErrRate == "0")

                                outfile.write(testing + ','+wpm + ',' + effic + ',' + UncErrRate + ',' + CorErrRate + ',' + AutoCorErrRate + ',' + ManCorErrRate + ',' + TotErrRate)

                                outfile.write('\n')

                fp.close()

outfile.close()

