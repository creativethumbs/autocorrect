using System;
using System.IO;
using System.Text;
using System.Collections;

namespace StreamAnalyzer
{
	public class CharLevel
	{		
		private readonly char[]	_charset;		// all the legal characters in the test phrases	
		
		private ProbTable		_macTable;		// probability table like the one by MacKenzie & Soukoreff
		private ProbTableEx		_wobTable;		// probability table like the one by Wobbrock & Myers
		private ConfusionMatrix	_macMatrix;		// confusion matrix from [1] (reflects P and T only)
		private ConfusionMatrix	_wobMatrix;		// confusion matrix from [4] (reflects P, T, and IS)

		public CharLevel()
		{
			// initialize the readonly character set with all
			// upper and lowercase alpha, numbers, and space
			int i = 0;
			_charset = new char[63];
			for (int ch = 'A'; ch <= 'Z'; ch++, i++)	// 26
				_charset[i] = (char) ch;
			for (int ch = 'a'; ch <= 'z'; ch++, i++)	// 26
				_charset[i] = (char) ch;
			for (int ch = '0'; ch <= '9'; ch++, i++)	// 10
				_charset[i] = (char) ch;
			_charset[i] = ' '; // space

			// initialize the probability tables with the character set
			_macTable = new ProbTable(_charset);
			_wobTable = new ProbTableEx(_charset);

			// initialize the confusion matrices with the character set
			_macMatrix = new ConfusionMatrix(_charset);
			_wobMatrix = new ConfusionMatrix(_charset);
		}

		// Clears the character level error analyzer of all its tallies.
		public void Clear()
		{
			_macTable.Clear();
			_wobTable.Clear();
			_macMatrix.Clear();
			_wobMatrix.Clear();
		}

		public void Analyze(Task t)
		{
			Analyze(null, t);
		}

		// Records all of the relevant character-level error analysis data  
		// for a single task phrase. This is the main method for this class.
		public void Analyze(TextWriter writer, Task t)
		{
			// first compute the string distance between P and T
			int[,] D = new int[t.P.Length + 1, t.T.Length + 1];
			float msd = MSD_Matrix(t, ref D); // fills D

			// create the alignments from the original analysis
			ArrayList pairs = new ArrayList();
			Align(t, D, t.P.Length, t.T.Length, String.Empty, String.Empty, ref pairs);
			float numAlign = pairs.Count / 2; // number of optimal alignment pairs

			if (writer != null) // write out unaligned P and T
			{
				writer.WriteLine("   P: {0}", t.P);			// presented string
				writer.WriteLine("   T: {0}", t.T);			// transcribed string
				writer.WriteLine(" MSD: {0}", msd);			// minimum string distance
				writer.WriteLine("#OPT: {0}", numAlign);	// number of optimal alignment pairs
			}

			// for each alignment pair, align P' and T' with IS and classify errors
			for (int i = 0; i < pairs.Count; i += 2)
			{
				string aP = (string) pairs[i];		// presented strings at (n*i)
				string aT = (string) pairs[i+1];	// transcribed strings at (n*i+1)
				InputStream aIS = new InputStream(t.IS);
				
				if (writer != null) // write out the aligned P' and T'
				{
					writer.WriteLine("\n P'{0}: {1}", i / 2, CharCoder.ToDisplay(aP));
					writer.WriteLine(" T'{0}: {1}", i / 2, CharCoder.ToDisplay(aT));
				}

				// update the counts of each presented and transcribed character
				_macTable.Count(aP, true, numAlign);
				_wobTable.Count(aP, true, numAlign);
				_wobTable.Count(aT, false, numAlign);

				// stream-align P' and T' with IS'
				aIS.StreamAlign(ref aP, ref aT);

				// ...conceptually, the position value assignment step is here...
				// but we handle it when an InputStream is created

				if (writer != null) // write out the stream-aligned P", T", and IS"
				{
					writer.WriteLine(" P\"{0}: {1}", i / 2, CharCoder.ToDisplay(aP));
					writer.WriteLine(" T\"{0}: {1}", i / 2, CharCoder.ToDisplay(aT));
					writer.WriteLine("IS\"{0}: {1}\n", i / 2, CharCoder.ToDisplay(aIS.ToString()));
				}

				// find and classify the errors
				DetermineErrors(writer, aP, aT, aIS, numAlign);
			}
			if (writer != null)
			{
				writer.WriteLine();
			}
		}

		public void Write(TextWriter writer)
		{
			_macTable.Write(writer);				// probability table from NordiCHI paper
			_wobTable.Write(writer, _wobMatrix);	// probability table from Wobbrock & Myers
			
			_macMatrix.Write("MacKenzie & Soukoreff P & T Confusion Matrix", writer);
			_wobMatrix.Write("Wobbrock & Myers P, T & IS Confusion Matrix", writer);
		}

		public static void Align(Task t, int[,] D, int x, int y, string aP, string aT, ref ArrayList pairs)
		{
			if (x == 0 && y == 0) 
			{
				pairs.Add(aP);
				pairs.Add(aT);
				return;
			}
			if (x > 0 && y > 0)
			{
				if (D[x,y] == D[x-1,y-1] && t.P[x-1] == t.T[y-1])
					Align(t, D, x-1, y-1, t.P[x-1] + aP, t.T[y-1] + aT, ref pairs);
				if (D[x,y] == D[x-1,y-1] + 1)
					Align(t, D, x-1, y-1, t.P[x-1] + aP, t.T[y-1] + aT, ref pairs);
			}
			if (x > 0 && D[x,y] == D[x-1,y] + 1)
				Align(t, D, x-1, y, t.P[x-1] + aP, (char) EncodeChar.InsertionOmission + aT, ref pairs); // omission
			
			if (y > 0 && D[x,y] == D[x,y-1] + 1)
				Align(t, D, x, y-1, (char) EncodeChar.InsertionOmission + aP, t.T[y-1] + aT, ref pairs); // insertion
		}

		private void DetermineErrors(TextWriter writer, string P, string T, InputStream IS, float numAlign)
		{
			for (int b = 0, a = 0; b < IS.UBound; b++)
			{
				if (T[b] == (char) EncodeChar.InsertionOmission)
				{
					RecordError(writer, ErrorType.UncorrectedOmission, P[b], (char) EncodeChar.InsertionOmission, numAlign);
					//// Attempting to solve the quickly<<y problem. Still a problem: qxickly<<<<<<y
					//int next = LookAhead(IS.ToString(), Math.Max(a,b), 0, new CharTest(IsNotSpacer));
					//if (next < IS.UBound && IS[next] == P[b])
					//{
					//	RecordError(writer, ErrorType.CorrectedNoError, P[b], IS[next], numAlign);
					//	a += (next - Math.Max(a,b) + 1);
					//}
				}
				else if (IS.IsFlagged(b) || b == IS.UBound - 1)
				{
					// for debugging - not necessary for algorithm
					string psub = P.Substring(a, b - a + 1);	//  p "chunk"
					string tsub = T.Substring(a, b - a + 1);	//  t "chunk"
					string isub = IS.Substring(a, b - a + 1);	// is "chunk"

					// declare oMission set, Insertion set
					ArrayList M = new ArrayList(b - a);
					ArrayList I = new ArrayList(b - a);

					// catalog corrected and nonrec errors in IS
					for (int i = a; i < b; i++)
					{
						int v = IS.GetPosition(i); // get the position value of IS[i]
						if (IS[i] == (char) EncodeChar.Backspace)
						{
							if (M.Contains(v))
								M.Remove(v);
							if (I.Contains(v))
								I.Remove(v);
						}
						else if (IS[i] != (char) EncodeChar.Spacer)
						{
							int target = LookAhead(P, b, v + M.Count - I.Count, new CharTest(IsLetter));
							if (IS[i] == (char) EncodeChar.Nonrecognition)
							{
								if (target >= P.Length)
									RecordError(writer, ErrorType.NonrecInsertion, (char) EncodeChar.InsertionOmission, (char) EncodeChar.Nonrecognition, numAlign);
								else
									RecordError(writer, ErrorType.NonrecSubstitution, P[target], (char) EncodeChar.Nonrecognition, numAlign);
							}
							else // IS[i] is a letter
							{
								int nextP = LookAhead(P, target, 1, new CharTest(IsLetter));
								int prevP = LookBehind(P, target, 1, new CharTest(IsLetter));
								int nextIS = LookAhead(IS.ToString(), i, 1, new CharTest(IsNotNRSpacer));
								int prevIS = LookBehind(IS.ToString(), i, 1, new CharTest(IsNotSpacer));
								
								if (target < P.Length && IS[i] == P[target])
								{
									RecordError(writer, ErrorType.CorrectedNoError, P[target], IS[i], numAlign);
								}
								else if (target >= P.Length 
									|| (nextIS < IS.UBound && IS[nextIS] == P[target]) 
									|| (prevIS >= 0 && prevP >= 0 && IS[prevIS] == IS[i] && IS[prevIS] == P[prevP]))
								{
									RecordError(writer, ErrorType.CorrectedInsertion, (char) EncodeChar.InsertionOmission, IS[i], numAlign);
									I.Add(v);
								}
								else if (nextP < P.Length && IS[i] == P[nextP] && IsLetter(T[target]))
								{
									RecordError(writer, ErrorType.CorrectedOmission, P[target], (char) EncodeChar.InsertionOmission, numAlign);
									RecordError(writer, ErrorType.CorrectedNoError, P[nextP], IS[i], numAlign);
									M.Add(v);
								}
								else 
								{
									RecordError(writer, ErrorType.CorrectedSubstitution, P[target], IS[i], numAlign);
								}
							}
						}
					}

					// catalog uncorrected insertions, uncorrected substitutions, and no errors next
					if (P[b] == (char) EncodeChar.InsertionOmission)
						RecordError(writer, ErrorType.UncorrectedInsertion, (char) EncodeChar.InsertionOmission, T[b], numAlign);
					else if (P[b] != T[b])
						RecordError(writer, ErrorType.UncorrectedSubstitution, P[b], T[b], numAlign);
					else if (P[b] != (char) EncodeChar.Spacer)
						RecordError(writer, ErrorType.UncorrectedNoError, P[b], T[b], numAlign);
					else if (IS[b] == (char) EncodeChar.Nonrecognition) // P[b] == T[b] == spacer
						RecordError(writer, ErrorType.NonrecInsertion, (char) EncodeChar.InsertionOmission, (char) EncodeChar.Nonrecognition, numAlign);

					// update the IS[a...b] substring range each loop
					a = b + 1;
				}
			}
		}

		private delegate bool CharTest(char ch);
		private bool IsLetter(char ch)
		{
			// this is only used on P, so we don't need to check for backspaces etc.
			return (ch != (char) EncodeChar.InsertionOmission && ch != (char) EncodeChar.Spacer);
		}
		private bool IsNotNRSpacer(char ch)
		{
			return (ch != (char) EncodeChar.Nonrecognition && ch != (char) EncodeChar.Spacer);
		}
		private bool IsNotSpacer(char ch)
		{
			return (ch != (char) EncodeChar.Spacer);
		}

		private int LookAhead(string S, int start, int count, CharTest condition)
		{
			int index = start;
			while (0 <= index && index < S.Length && !condition(S[index]))
				index++;
			while (count > 0 && index < S.Length)
			{
				if (++index >= S.Length)
					break;
				else if (condition(S[index]))
					count--;
			}
			return index;
		}

		private int LookBehind(string S, int start, int count, CharTest condition)
		{
			int index = start;
			while (0 <= index && index < S.Length && !condition(S[index]))
				index--;
			while (count > 0 && index >= 0)
			{
				if (--index < 0)
					break;
				else if (condition(S[index]))
					count--;
			}
			return index;
		}

		/// <summary>
		/// Records an error of the given error type for the character pair (intended, produced).
		/// Substitution errors of this type get entered into the confusion matrix. If a produced
		/// character falls outside the character set, it is entered in the bottom row of the
		/// confusion matrix, the miscellany row.
		/// </summary>
		/// <param name="err">The character-level error type.</param>
		/// <param name="intended">The character most likely intended by the user.</param>
		/// <param name="produced">The character actually produced by the user.</param>
		private void RecordError(TextWriter writer, ErrorType err, char intended, char produced, float numAlign)
		{
			// confusion matrix entries
			if (err == ErrorType.UncorrectedNoError || err == ErrorType.UncorrectedSubstitution)
			{
				_macMatrix.AddEntry(intended, produced, numAlign);
				_wobMatrix.AddEntry(intended, produced, numAlign);
			}
			else if (err == ErrorType.CorrectedNoError 
				|| err == ErrorType.CorrectedSubstitution 
				|| err == ErrorType.NonrecSubstitution)
			{
				// the MacKenzie matrix does not get non-transcribed chars
				_wobMatrix.AddEntry(intended, produced, numAlign);
			}
			
			// probability table entries
			if (err == ErrorType.UncorrectedNoError
				|| err == ErrorType.CorrectedNoError
				|| err == ErrorType.UncorrectedOmission
				|| err == ErrorType.CorrectedOmission
				|| err == ErrorType.UncorrectedSubstitution
				|| err == ErrorType.CorrectedSubstitution
				|| err == ErrorType.NonrecSubstitution)
			{
				// the omissions and substitutions have their intended characters added
				_macTable.AddEntry(err, intended, numAlign);
				_wobTable.AddEntry(err, intended, numAlign);
			}
			else if (err == ErrorType.UncorrectedInsertion 
				|| err == ErrorType.CorrectedInsertion 
				|| err == ErrorType.NonrecInsertion)
			{
				// the insertions have their produced characters added
				_macTable.AddEntry(err, produced, numAlign);
				_wobTable.AddEntry(err, produced, numAlign);
			}

			if (writer != null)
			{
				writer.WriteLine("{0}({1},{2})", err, intended, produced);
			}
		}

		#region String Distance

		// returns just the string distance, not the filled msd matrix
		public static int StringDistance(Task t)
		{
			int[,] D = new int[t.P.Length + 1, t.T.Length + 1];
			return MSD_Matrix(t, ref D);
		}

		// returns string distance and the matrix generated in the process
		public static int MSD_Matrix(Task t, ref int[,] D)
		{
			for (int i = 0; i <= t.P.Length; i++)
				D[i,0] = i;
			
			for (int j = 0; j <= t.T.Length; j++)
				D[0,j] = j;

			for (int i = 1; i <= t.P.Length; i++)
				for (int j = 1; j <= t.T.Length; j++)
					D[i,j] = Min3(D[i-1,j] + 1, D[i,j-1] + 1, D[i-1,j-1] + r(t.P[i-1], t.T[j-1]));

			return D[t.P.Length, t.T.Length];
		}

		/// <summary>
		/// Returns the minimum of 3 integers.
		/// </summary>
		private static int Min3(int n, int m, int k) 
		{
			int a = Math.Min(n, m);
			if (a < k)
				return a;
			else
				return k;
		}

		/// <summary>
		/// Helper function 'r' defined in [2].
		/// </summary>
		private static int r(char x, char y) 
		{
			if (x == y)
				return 0;
			else
				return 1;
		}

		#endregion
	}
}
