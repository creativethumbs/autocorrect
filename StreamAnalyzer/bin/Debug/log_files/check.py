import random,os,re
from itertools import cycle

txtfolder = '/Users/tichaseth/Documents/MSCS F17/Autocorrection/autocorrect/StreamAnalyzer/bin/Debug/log_files'

for filename in os.listdir(txtfolder):
    if filename.endswith(".xml"):
        infile = os.path.join(txtfolder, filename)

        with open(infile) as fp:
            closecount = 0
            for i,line in enumerate(fp):
                if '<close' in line:
                    closecount += 1
            assert(closecount == 1), "multiple close in file: %r" % filename

print "All files clean!"