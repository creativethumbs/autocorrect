using System;
using System.IO;

namespace StreamAnalyzer
{
	public class ConfusionMatrix
	{
		private char[]			_charset;
		private float[,]		_matrix;

		private readonly int	_nonrecIndex;
		private readonly int	_miscIndex;

		public ConfusionMatrix(char[] chars)
		{
			// build our confusion matrix off the character set passed in.
			_charset = new char[chars.Length + 2];
			chars.CopyTo(_charset, 0);

			// add non-recognition to charset
			_nonrecIndex = chars.Length;
			_charset[_nonrecIndex] = (char) EncodeChar.Nonrecognition;

			// add wildcard miscellaneous to charset
			_miscIndex = chars.Length + 1;
			_charset[_miscIndex] = (char) EncodeChar.Miscellaneous;

			// make the confusion matrix itself [intended, produced]
			_matrix = new float[_charset.Length, _charset.Length];
		}

		public void Clear()
		{
			Array.Clear(_matrix, 0, _matrix.Length);
		}

		public void AddEntry(char intended, char produced, float numAlign)
		{
			int iIntended = CharToIndex(intended);
			int iProduced = CharToIndex(produced);
			_matrix[iIntended, iProduced] += (1 / numAlign);
		}

		// find the index of a given character in the character set. if it is
		// not found, then the miscellaneous index is returned.
		private int CharToIndex(char ch)
		{
			// iterate over everything but the miscellaneous entry
			for (int i = 0; i < _charset.Length - 1; i++)
			{
				if (_charset[i] == ch)
					return i;
			}

			return _miscIndex;
		}
		
		// sums up everything in the matrix
		public float Total
		{
			get
			{
				float total = 0f;
				for (int y = 0; y < _charset.Length; y++)
				{
					for (int x = 0; x < _charset.Length; x++)
					{
						total += _matrix[x,y];
					}
				}
				return total;
			}
		}

		// gets the total number of produced characters. non-recognitions are
		// excluded, since no character is produced for them.
		public float TotalEntered 
		{
			get
			{
				float total = 0f;
				for (int y = 0; y < _charset.Length; y++)
				{
					for (int x = 0; x < _charset.Length; x++)
					{
						if (x != _nonrecIndex && y != _nonrecIndex)
							total += _matrix[x,y];
					}
				}
				return total;
			}
		}

		// sums up the diagonal
		public float TotalCorrect
		{
			get
			{
				float total = 0f;
				for (int x = 0; x < _charset.Length; x++)
					total += _matrix[x,x];
				return total;
			}
		}

		public float TotalIncorrect
		{
			get
			{
				return Total - TotalCorrect;
			}
		}

		public float TotalNonrecs
		{
			get
			{
				float total = 0f;
				for (int x = 0; x < _charset.Length; x++)
					total += _matrix[x, _nonrecIndex];
				return total;
			}
		}

		// sums the column in the confusion matrix
		public float GetNumIntended(char ch)
		{
			float total = 0f;
			int x = CharToIndex(ch);

			for (int y = 0; y < _charset.Length; y++)
				total += _matrix[x,y];

			return total;
		}

		// sums the row in the confusion matrix. note that non-recognitions are
		// never intended, and thus are never in the right-most column for a 
		// letter.
		public float GetNumEntered(char ch)
		{
			float total = 0F;
			int y = CharToIndex(ch);

			for (int x = 0; x < _charset.Length; x++)
				total += _matrix[x,y];

			return total;
		}

		public float GetNumCorrect(char ch)
		{
			int x = CharToIndex(ch);
			return _matrix[x,x];
		}

		public float GetNumIncorrect(char ch)
		{
			float attempts = GetNumIntended(ch);
			float correct = GetNumCorrect(ch);
			return (attempts - correct);
		}

		public float GetNumNonRecs(char ch)
		{
			int x = CharToIndex(ch);
			return _matrix[x, _nonrecIndex];
		}

		public float GetNumMisc(char ch)
		{
			int x = CharToIndex(ch);
			return _matrix[x, _miscIndex];
		}

		public float GetProbCorrect(char ch)
		{
			float attempts = GetNumIntended(ch);
			float correct = GetNumCorrect(ch);
			return (attempts > 0f ? correct / attempts : 0f);
		}

		public float GetProbIncorrect(char ch)
		{
			float attempts = GetNumIntended(ch);
			float incorrect = GetNumIncorrect(ch);
			return (attempts > 0f ? incorrect / attempts : 0f);
		}

		public float GetProbNonRec(char ch)
		{
			float attempts = GetNumIntended(ch);
			float nrs = GetNumNonRecs(ch);
			return (attempts > 0f ? nrs / attempts : 0f);
		}

		public float GetProbMisc(char ch)
		{
			float attempts = GetNumIntended(ch);
			float misc = GetNumMisc(ch);
			return (attempts > 0f ? misc / attempts : 0f);
		}

		/// <summary>
		/// Writes the confusion matrix to a file using the given stream writer. The matrix
		/// is space-delimited for easy pasting or importing to a spreadsheet in a stats
		/// package or Microsoft Excel. The X-axis defines the intended characters, and
		/// the Y-axis defines the characters actually produced for each intended character.
		/// </summary>
		public void Write(string title, TextWriter writer)
		{
			writer.WriteLine();
			writer.WriteLine("-- {0} --", title);
			writer.WriteLine("AXES: X=intended, Y=produced.");
			writer.WriteLine();
			
			for (int x = 0; x < _charset.Length; x++)
			{
				writer.Write("\t{0}", _charset[x]);
			}
			writer.WriteLine();

			for (int y = 0; y < _charset.Length; y++)
			{
				writer.WriteLine();
				writer.Write("{0}", _charset[y]);
				for (int x = 0; x < _charset.Length; x++) 
				{
					writer.Write("\t{0:f3}", _matrix[x,y]);
				}
			}

			writer.WriteLine();
		}
	}
}
