using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections;
using System.Diagnostics;
using System.Reflection;

namespace StreamAnalyzer
{
    public class Analyzer
    {
        public Analyzer()
        {
        }
        // don't want the analyzer to just rage quit when the task fails
        bool taskfailed = false;

        // main analysis method for direct (P,IS) entry
        public void Analyze(string P, string IS, string outfile)
        {
            CharLevel charlvl = new CharLevel();
            
            Task t = new Task();
            t.P = P;
            t.IS = new InputStream(IS);
            t.T = t.IS.Transcription;
            
            TextWriter writer = null;
            if (outfile != String.Empty)
            {
                writer = new StreamWriter(outfile, false, Encoding.UTF8);
            }
            else
            {
                writer = Console.Out;
            }

            // patch into the character-level error analyzer
            charlvl.Analyze(writer, t);
        }

        #region Logfile Reading

        // main analysis loop for log file analysis
        public bool Analyze(string xmlFile, string txtFile)
        {
            bool success = true;
            XmlTextReader reader = null;
            StreamWriter writer = null;
            try
            {
                // open the xml file for reading, the text file for writing
                reader = new XmlTextReader(new StreamReader(xmlFile, Encoding.UTF8));
                reader.WhitespaceHandling = WhitespaceHandling.None;
                writer = new StreamWriter(txtFile, false, Encoding.UTF8);

                // initiate a new character-level data analyzer
                CharLevel charlvl = new CharLevel();

                // move to the content and read the root node
                reader.IsStartElement("TextTest");

                // write the output file header
                WriteHeader(writer);

                // main parsing loop
                reader.Read();
                while (reader.LocalName == "task")
                {
                    taskfailed = false;
                    Task t = new Task();
                    ReadOneTask(reader, t);
                    if (t.IS.Characters > 0)
                    {
                        WriteOneTask(writer, t); 
                        charlvl.Analyze(t); // record character-level error data
                    }
                    if (success && taskfailed) { 
                        success = false;
                    }

                    reader.Read();
                }
                reader.ReadStartElement("close");
                
                // write the character-level results all at once. this must be done
                // at the end since we must have seen all characters that were entered.
                charlvl.Write(writer);
            }
            catch (FileNotFoundException fnfex)
            {
                Console.WriteLine(fnfex.Message);
                success = false;
            }
            catch (XmlException xex)
            {
                Console.WriteLine(xex.Message);
                success = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                success = false;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (writer != null)
                    writer.Close();
            }

            return success;
        }

        /// <summary>
        /// Reads a single task block of XML.
        /// </summary>
        /// <param name="t"></param>
        private void ReadOneTask(XmlTextReader reader, Task t)
        {
            t.Num = XmlConvert.ToInt32(reader.GetAttribute("number"));
            t.IsTest = XmlConvert.ToBoolean(reader.GetAttribute("testing").ToLower());
            
            reader.Read();
            t.P = reader.ReadElementString("presented");

            double time;
            double tmSave = 0d;
            bool bJustUp = false;
            string stream = String.Empty;
            
            while (reader.LocalName != "transcribed")
            {
                switch (reader.LocalName)
                {
                    case "char_start":
                        if (bJustUp)
                        {
                            // no entry or nonrec event followed the last down/up pair 
                            // so add an implicit non-recognition to the input stream
                            stream += (char) EncodeChar.Nonrecognition;
                            bJustUp = false; // unset
                        }
                        time = XmlConvert.ToDouble(reader.GetAttribute("time"));
                        if (t.Init == 0.0) // initial time of activity for this task
                            t.Init = time;
                        if (tmSave != 0.0)
                            t.Inter += (time - tmSave); // this down time minus last up time
                        tmSave = time; // save down time
                        break;
                    
                    case "char_end":
                        time = XmlConvert.ToDouble(reader.GetAttribute("time"));
                        t.Intra += (time - tmSave); // this up time minus last down time
                        tmSave = time; // save up time
                        bJustUp = true;
                        break;
                    
                    case "entry":
                        stream += (char) XmlConvert.ToUInt16(reader.GetAttribute("value"));
                        bJustUp = false; // unset
                        
                        // task durations measured between entered characters only
                        time = XmlConvert.ToDouble(reader.GetAttribute("time"));
                        if (t.Init == 0.0)    // initial time of any activity for this task
                            t.Init = time;
                        if (time < t.Start)    // start time for this task - first character
                            t.Start = time;
                        if (time > t.End)    // end time for this task - last character
                            t.End = time;
                        break;
                    
                    case "nonrec":
                        time = XmlConvert.ToDouble(reader.GetAttribute("time"));
                        if (t.Init == 0.0)    // initial time of any activity for this task
                            t.Init = time;
                        stream += (char) EncodeChar.Nonrecognition;
                        bJustUp = false; // unset
                        break;
                }
                reader.Read();
            }

            // construct the input stream object from the string
            t.IS = new InputStream(stream);

            // read between <transcribed></transcribed> and advance
            t.T = reader.ReadElementString("transcribed");
            
            // be sure the input stream produces the recorded transcription
            if (t.IS.Transcription != t.T)
            {
                taskfailed = true;
				System.Console.Write("Input stream does not form transcription (task " + t.Num + " line " + reader.LineNumber + " pos " + reader.LinePosition + ")");

            }
        }

        #endregion

        #region Outfile Writing

        /// <summary>
        /// Writes out the aggregate text entry measures for one task. (Character-level measures,
        /// on the other hand, are written out by another class.) The data is a space-delimited 
        /// line in a text file that can be copied-and-pasted into your favorite statistics package 
        /// (JMP, Minitab, SPSS). Note that the task data is stored in the given task, and processed 
        /// as needed by each of the functions, which represent one statistic.
        /// </summary>
        /// <param name="t">The task data to be processed by each statistic.</param>
        /// <remarks>Since objects are passed by reference, if a statistic were to chnage the values
        /// in the Task parameter, it would affect subsequent statistics. Statistics should thus
        /// not affect the values in task.</remarks>
        private void WriteHeader(StreamWriter writer)
        {
            Version v = Assembly.GetExecutingAssembly().GetName().Version;
            writer.WriteLine("Character-Level Error Analyzer {0}.{1}.{2} (C) 2005 Carnegie Mellon University", v.Major, v.Minor, v.Build);
            writer.WriteLine("Software written in C# .NET by Jacob O. Wobbrock <jrock@cs.cmu.edu> and adapted by Ticha Sethapakdi");
            writer.WriteLine("Algorithms by R.W. Soukoreff, I.S. MacKenzie, J.O. Wobbrock and B.A. Myers");
            writer.WriteLine("Options: -d allows direct entry of P and IS.\n");
            writer.WriteLine("Num Testing? Time WPM AdjWPM CPS KSPS GPS Intra Inter StrDist MSD_v1 MSD_v2 MSD_v3 KSPC_v1 KSPC_v3 GPC GPKS UncErrRate CorErrRate AutoCorErrRate ManCorErrRate TotErrRate Effic Consc UtilBand WasteBand |C| |INF| |IF| |AIF| |MIF| |F| |P| |T| |IS| |G| |NR|\n");
        }

        private void WriteOneTask(StreamWriter writer, Task t)
        {
            writer.WriteLine("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14} {15} {16} {17} {18} {19} {20} {21} {22} {23} {24} {25} {26} {27} {28} {29} {30} {31} {32} {33} {34} {35} {36} {37}",
                t.Num,                        // task number
                t.IsTest ? 1 : 0,            // testing?
                
                // speed
                t.Time,                        // duration of task
                WPM(t),                        // words per minute
                AdjWPM(t),                    // words per minute, adjusted for uncorrected errors
                CPS(t),                        // characters per second, |T| / time
                KSPS(t),                    // keystrokes per second, |IS| / time
                GPS(t),                        // gestures per second, pen downs / time
                IntraTime(t),                // average intra-character time (duration of characters)
                InterTime(t),                // average inter-character time (duration between characters)
                
                // accuracy
                CharLevel.StringDistance(t),// minimum string distance, |P| and |T|
                MSD_v1(t),                    // MSD error rate, definition 1
                MSD_v2(t),                    // MSD error rate, definition 2
                MSD_v3(t),                    // MSD error rate, definition 3
                Math.Round(t.IS.KSPC, 3),    // keystrokes per character, definition from [2]
                KSPC(t),                    // keystrokes per character, definition from [3]
                Math.Round(t.IS.GPC, 3),    // gestures per character (pen downs / |T|)
				Math.Round(t.IS.GPKS, 3),    // gestures per keystrokes (pen downs / |IS|)
				UncorrectedErrorRate(t),    // rate of errors left in |T|
                CorrectedErrorRate(t),        // rate of errors made in |IS| but fixed by backspace
                AutoCorrectedErrorRate(t),        // rate of errors made in |IS| but fixed by autocorrect
                ManualCorrectedErrorRate(t),        // rate of errors made in |IS| but fixed by manualcorrect
				TotalErrorRate(t),            // combined rate of errors
                
                // other measures
                CorrectionEfficiency(t),
                Conscientiousness(t),
                UtilizedBandwidth(t),
                WastedBandwidth(t),
                
                // character classes
                Correct(t),                   // |C|
                IncorrectNotFixed(t),         // |INF|
                IncorrectFixed(t),            // |IF|
                AutoIncorrectFixed(t),        // |AIF|
                ManualIncorrectFixed(t),      // |MIF|
				Fixes(t),                     // |F|
                
                // string measures
                t.P.Length,                    // |P|
                t.T.Length,                    // |T|
                t.IS.Length,                // |IS| (includes backspaces, excludes non-recs)
                t.IS.Gestures,                // |G| = pen downs
                t.IS.Nonrecs                // |NR|
                );
        }

        #endregion

        #region Speed
        
        /// <summary>
        /// Standard words per minute measure. A word is 5 characters, including spaces.
        /// Since we start the timing on the first character entered, we must substract
        /// one from the length so as not to count the first character. See
        /// http://www.yorku.ca/mack/RN-TextEntrySpeed.html
        /// </summary>
        public float WPM(Task t)
        {
            if (t.Time == 0)
                return 0;

            float wpm = ((t.T.Length - 1) / t.Time) * 60f * 0.2f;
            return (float) Math.Round(wpm, 3);
        }

        /// <summary>
        /// Adjusted words per minute, defined by reducing the speed proportional to
        /// the number of errors left in the transcribed string. We aren't sure if this
        /// is a standard definition or not; we "invented" this ourselves, though I think
        /// it must have been defined previously.
        /// </summary>
        public float AdjWPM(Task t)
        {
            if (t.Time == 0)
                return 0;
            
            float wpm = ((t.T.Length - 1) / t.Time) * 60f * 0.2f;
            float unc = UncorrectedErrorRate(t);
            float adj = wpm * (1f - unc);
            return (float) Math.Round(adj, 3);
        }

        /// <summary>
        /// Standard characters per second measure. Redundant with WPM.
        /// </summary>
        public float CPS(Task t)
        {
            if (t.Time == 0)
                return 0;
            
            float cps = (t.T.Length - 1) / t.Time;
            return (float) Math.Round(cps, 3);
        }

        /// <summary>
        /// Keystrokes per second. This measure cares nothing for the length of the 
        /// final transcription, as WPM and CPS must. This is a measure of a raw data 
        /// transfer rate, used for computing upper bounds on human performance (i.e., 
        /// assume all characters are correct). The stream length itself is used, so 
        /// backspaces count as much as all other characters. This metric was described 
        /// in [4] as 'BPS'.
        /// </summary>
        public float KSPS(Task t)
        {
            if (t.Time == 0)
                return 0;
            
            float ctps = (t.IS.Length - 1) / t.Time;
            return (float) Math.Round(ctps, 3);
        }

        /// <summary>
        /// Gestures per second. This is like CTPS except that it includes all gestures
        /// in the input stream, so nonrecognitions, mode-setters, and other gestures
        /// should be included, not just transmitted bytes. 
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public float GPS(Task t)
        {
            if (t.End - t.Init == 0)
                return 0;
            
            float gps = t.IS.Gestures / (float) (t.End - t.Init);
            return (float) Math.Round(gps, 3);
        }

        /// <summary>
        /// The duration of characters can be called the intra-character time,
        /// which is the time from pen-down to pen-up for a gesture. By using 
        /// the input stream's length, we don't include non-recs in the division
        /// because they can be misleading, as many are just taps on the screen.
        /// </summary>
        /// <returns>The average intra-character time for Task t.</returns>
        public float IntraTime(Task t)
        {
            return (float) (t.Intra / t.IS.Length);
        }

        /// <summary>
        /// The time in between characters can be called the inter-character time,
        /// which is the time from pen-up for one gesture to pen-down to the
        /// next character-producing gesture.
        /// </summary>
        /// <returns>The average inter-character time for Task t.</returns>
        public float InterTime(Task t)
        {
            return (float) (t.Inter / t.IS.Length);
        }

        #endregion

        #region Accuracy

        /// <summary>
        /// This MSD metric is defined in [2].
        /// </summary>
        public float MSD_v1(Task t)
        {
            float msd = CharLevel.StringDistance(t);
            float max = Math.Max(t.P.Length, t.T.Length);
            return (float) Math.Round(msd / max, 3);
        }

        /// <summary>
        /// This MSD error metric is defined in [1]. It computes the average length of the
        /// alignments.
        /// </summary>
        public float MSD_v2(Task t)
        {
            int[,] D = new int[t.P.Length + 1, t.T.Length + 1];
            float msd = CharLevel.MSD_Matrix(t, ref D);
            
            ArrayList pairs = new ArrayList();
            CharLevel.Align(t, D, t.P.Length, t.T.Length, String.Empty, String.Empty, ref pairs);
            
            float length = 0F;
            for (int i = 0; i < pairs.Count; i++)
            {
                length += ((string) pairs[i]).Length;
            }
            float meanlen = length / pairs.Count;    // mean length of the alignments
            return (float) Math.Round(msd / meanlen, 3);
        } 

        /// <summary>
        /// This MSD error rate is defined in [3].
        /// </summary>
        public float MSD_v3(Task t)
        {
            float inf = IncorrectNotFixed(t);
            float c = Correct(t);
            float msd = inf / (inf + c);
            return (float) Math.Round(msd, 3);
        }

		/// <summary>
		/// This KSPC metric is defined in [3].
		/// </summary>
		// NOTE: do we need to add aifx,mifx?
		public float KSPC(Task t)
        {
            float c = Correct(t);
            float inf = IncorrectNotFixed(t);
            float ifx = IncorrectFixed(t);
            float fixes = Fixes(t);
            float kspc = (c + inf + ifx + fixes) / (c + inf);
            return (float) Math.Round(kspc, 3);
        }

        public float UncorrectedErrorRate(Task t)
        {
            float c = Correct(t);
            float inf = IncorrectNotFixed(t);
            float ifx = IncorrectFixed(t);
			float aifx = AutoIncorrectFixed(t);
			float mifx = ManualIncorrectFixed(t);
            float unc = inf / (c + inf + ifx + aifx + mifx);
            return (float) Math.Round(unc, 3);
        }

        // TODO: change this after adding functions
        public float CorrectedErrorRate(Task t)
        {
            float c = Correct(t);
            float inf = IncorrectNotFixed(t);
            float ifx = IncorrectFixed(t);
            float aifx = AutoIncorrectFixed(t);
            float mifx = ManualIncorrectFixed(t);
            float cor = ifx / (c + inf + ifx + aifx + mifx);
            return (float) Math.Round(cor, 3);
        }
        public float AutoCorrectedErrorRate(Task t)
        {
            float c = Correct(t);
            float inf = IncorrectNotFixed(t);
            float ifx = IncorrectFixed(t);
            float aifx = AutoIncorrectFixed(t);
            float mifx = ManualIncorrectFixed(t);
            float cor = aifx / (c + inf + ifx + aifx + mifx);
            return (float)Math.Round(cor, 3);
        }
		public float ManualCorrectedErrorRate(Task t)
		{
			float c = Correct(t);
			float inf = IncorrectNotFixed(t);
			float ifx = IncorrectFixed(t);
			float aifx = AutoIncorrectFixed(t);
			float mifx = ManualIncorrectFixed(t);
			float cor = mifx / (c + inf + ifx + aifx + mifx);
			return (float)Math.Round(cor, 3);
		}
        // TODO: look at this
        public float TotalErrorRate(Task t)
        {
            float c = Correct(t);
            float inf = IncorrectNotFixed(t);
            float ifx = IncorrectFixed(t);
            float aifx = AutoIncorrectFixed(t);
            float mifx = ManualIncorrectFixed(t);
            float tot = (inf + ifx + aifx + mifx) / (c + inf + ifx + aifx + mifx);
            return (float) Math.Round(tot, 3);
        }

        #endregion

        #region Other Measures

        public float CorrectionEfficiency(Task t)
        {
            float ifx = IncorrectFixed(t);
            float aifx = AutoIncorrectFixed(t);
            float mifx = ManualIncorrectFixed(t);
            float fixes = Fixes(t);
            float autofixes = AutoFixes(t);
            float manualfixes = ManualFixes(t);
            return (fixes > 0F ? (float) Math.Round((ifx + aifx + mifx) / (fixes + autofixes + manualfixes), 3) : 1f);
        }

        public float Conscientiousness(Task t)
        {
            float ifx = IncorrectFixed(t);
            float inf = IncorrectNotFixed(t);
            return (ifx + inf > 0F ? (float) Math.Round(ifx / (ifx + inf), 3) : 1F);
        }

        public float UtilizedBandwidth(Task t)
        {
            float c = Correct(t);
            float inf = IncorrectNotFixed(t);
            float ifx = IncorrectFixed(t);
            float fixes = Fixes(t);
            float uband = c / (c + inf + ifx + fixes);
            return (float) Math.Round(uband, 3);
        }

        public float WastedBandwidth(Task t)
        {
            float c = Correct(t);
            float inf = IncorrectNotFixed(t);
            float ifx = IncorrectFixed(t);
            float fixes = Fixes(t);
            float waste = (inf + ifx + fixes) / (c + inf + ifx + fixes);
            return (float) Math.Round(waste, 3);
        }

		#endregion

		#region Character Classes

		// only counts character 8
		public int Fixes(Task t)
        { 
            return t.IS.Backspaces;
        }

        // counts character 10
        public int AutoFixes(Task t)
        { 
            return t.IS.AutoWordBackspaces;
        }

		// counts character 9
		public int ManualFixes(Task t)
		{ 
			return t.IS.WordBackspaces;
		} 

        public int IncorrectNotFixed(Task t)
        {
            string pres = t.P;

            if(pres.Equals(t.T)) {
                Debug.Assert(CharLevel.StringDistance(t) == 0);
            }

            // string distance between presented string and transcribed string
            return CharLevel.StringDistance(t);
        }

        public int Correct(Task t)
        {
            return Math.Max(t.P.Length, t.T.Length) - CharLevel.StringDistance(t);
        }

		// only consider normal (character 8)
		public int IncorrectFixed(Task t)
		{ 
            return t.IS.Length - t.T.Length - Fixes(t) - (t.IS.AutoWordDeleted + t.IS.ManualWordDeleted +t.IS.Insertions + t.IS.AutoWordBackspaces + t.IS.WordBackspaces);
		}

        // only consider string distnaces (character 10)
        public int AutoIncorrectFixed(Task t)
        {
            return t.IS.AutoDistances;
        }

		// only consider string distnaces (character 9)
		public int ManualIncorrectFixed(Task t)
		{
            return t.IS.ManualDistances;
		}


        #endregion
    }
}
