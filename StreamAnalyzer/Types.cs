using System;

namespace StreamAnalyzer
{
    // TODO: add manual word backspace: 9
    // auto word backspace: 10
    // Manual correct: 11
    // autocorrect: 12
	public enum EncodeChar : byte
	{
		Backspace			= 8,	// '\b'
		ManualWordBackspace	= 9,    // at the start of the word that the user selected from a list
		AutoWordBackspace   = 10,   // at the start of an autocorrected word
		ManualInsert       = 11,   // at the end of the word that the user selected from a list
        AutoInsert         = 12,   // at the end of an autocorrected word
		InsertionOmission	= 183,	// '�'
		Spacer				= 175,	// '�'
		Nonrecognition		= 248,	// '�'
		Miscellaneous		= 164	// '�'
	}

	public enum DisplayChar : byte
	{
		Backspace			= 60,	// '<'
		ManualWordBackspace = 62,   // '>'
        AutoWordBackspace   = 35,   // '#'
		ManualInsert       = 38,   // '&'
		AutoInsert         = 37,   // '%'
		InsertionOmission	= 45,	// '-'
		Spacer				= 95,	// '_'
		Nonrecognition		= 42,	// '*'
		Miscellaneous		= 164	// '�'
	}

	public class CharCoder
	{
		// convert from an encoded string to a display string
		public static string ToDisplay(string s)
		{
			s = s.Replace((char) EncodeChar.Backspace, (char) DisplayChar.Backspace);
			s = s.Replace((char) EncodeChar.ManualWordBackspace, (char) DisplayChar.ManualWordBackspace);
            s = s.Replace((char)EncodeChar.AutoWordBackspace, (char)DisplayChar.AutoWordBackspace);
            s = s.Replace((char)EncodeChar.ManualInsert, (char)DisplayChar.ManualInsert);
            s = s.Replace((char)EncodeChar.AutoInsert, (char)DisplayChar.AutoInsert);
			s = s.Replace((char) EncodeChar.InsertionOmission, (char) DisplayChar.InsertionOmission);
			s = s.Replace((char) EncodeChar.Spacer, (char) DisplayChar.Spacer);
			s = s.Replace((char) EncodeChar.Nonrecognition, (char) DisplayChar.Nonrecognition);
			return s;
		}

		public static string ToEncoded(string s)
		{
			s = s.Replace((char) DisplayChar.Backspace, (char) EncodeChar.Backspace);
			s = s.Replace((char) DisplayChar.ManualWordBackspace, (char) EncodeChar.ManualWordBackspace);
			s = s.Replace((char)DisplayChar.AutoWordBackspace, (char)EncodeChar.AutoWordBackspace);
			s = s.Replace((char)DisplayChar.ManualInsert, (char)EncodeChar.ManualInsert);
			s = s.Replace((char)DisplayChar.AutoInsert, (char)EncodeChar.AutoInsert);
			s = s.Replace((char) DisplayChar.InsertionOmission, (char) EncodeChar.InsertionOmission);
			s = s.Replace((char) DisplayChar.Spacer, (char) EncodeChar.Spacer);
			s = s.Replace((char) DisplayChar.Nonrecognition, (char) EncodeChar.Nonrecognition);
			return s;
		}
	}

	public enum ErrorType
	{
		UncorrectedNoError = 0,
		CorrectedNoError,
			
		UncorrectedOmission,
		UncorrectedInsertion,
		UncorrectedSubstitution,

		CorrectedOmission,
		CorrectedInsertion,
		CorrectedSubstitution,
			
		NonrecInsertion,
		NonrecSubstitution,
			
		NumErrorTypes	// 10
	}
}
