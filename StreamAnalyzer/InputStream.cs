using System;
using System.Diagnostics;
using System.Collections;

namespace StreamAnalyzer
{
    public sealed class InputStream
    {
        private BitArray _flags;
        private ArrayList _positions;
        private string _stream;

        #region Fundamental

        public InputStream(string s)
        {
            _stream = s;
            _flags = new BitArray(s.Length);

            _positions = new ArrayList(s.Length);
            for (int i = 0; i < _positions.Capacity; i++)
                _positions.Add(0); // initialize

            FlagStream();
            AssignPositions();
        }

        // copy ctor
        public InputStream(InputStream s)
        {
            _flags = new BitArray(s._flags.Count);
            for (int i = 0; i < s._flags.Count; i++)
                _flags[i] = s._flags[i];

            _positions = new ArrayList(s._positions.Count);
            for (int j = 0; j < s._positions.Count; j++)
                _positions.Add((int)s._positions[j]);

            _stream = s._stream;
        }

        // use CharCoder.ToDisplay(IS) to make this string human-readable
        public override string ToString()
        {
            return _stream;
        }

        public string Substring(int start, int length)
        {
            return _stream.Substring(start, length);
        }

        public char this[int index]
        {
            get
            {
                return _stream[index];
            }
        }

        #endregion

        #region Metrics

        public float KSPC
        {
            get
            {
                float isLen = this.Length;
                float tLen = Transcription.Length;
                float kspc = isLen / tLen;
                return kspc;
            }
        }

        // gestures per (transcribed) character
        public float GPC
        {
            get
            {
                float isGes = this.Gestures;
                float tLen = Transcription.Length;
                float gpc = isGes / tLen;
                return gpc;
            }
        }

        // gestures per keystroke
        public float GPKS
        {
            get
            {
                float isGes = this.Gestures;
                float isLen = this.Length;
                float gptc = isGes / isLen;
                return gptc;
            }
        }

        #endregion

        #region Exclusive Counts

        public int Characters
        {
            get
            {
                int num = 0;
                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] != (char)EncodeChar.Backspace
                        && _stream[i] != (char)EncodeChar.ManualWordBackspace
                        && _stream[i] != (char)EncodeChar.AutoWordBackspace
                        && _stream[i] != (char)EncodeChar.ManualInsert
                        && _stream[i] != (char)EncodeChar.AutoInsert
                        && _stream[i] != (char)EncodeChar.Nonrecognition
                        && _stream[i] != (char)EncodeChar.Spacer)
                    {
                        num++;
                    }
                }
                return num;
            }
        }

        public int Length
        {
            get
            {
                int num = 0;
                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] != (char)EncodeChar.Nonrecognition
                        && _stream[i] != (char)EncodeChar.Spacer)
                    {
                        num++;
                    }
                }
                return num;
            }
        }

        public int Gestures
        {
            get
            {
                int num = 0;
                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] != (char)EncodeChar.Spacer)
                        num++;
                }
                return num;
            }
        }

        public int UBound
        {
            get
            {
                return _stream.Length;
            }
        }

        #endregion

        #region Inclusive Counts

        public int Nonrecs
        {
            get
            {
                int num = 0;
                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] == (char)EncodeChar.Nonrecognition)
                        num++;
                }
                return num;
            }
        }

        public int Backspaces
        {
            get
            {
                int num = 0;
                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] == (char)EncodeChar.Backspace)
                        num++;
                }
                return num;
            }
        }

        public int WordBackspaces
        {
            get
            {
                int num = 0;
                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] == (char)EncodeChar.ManualWordBackspace)
                        num++;
                }
                return num;
            }
        }

        public int Insertions
        {
            get
            {
                int num = 0;
                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] == (char)EncodeChar.AutoInsert ||
                        _stream[i] == (char)EncodeChar.ManualInsert)
                        num++;
                }
                return num;
            }
        }

        public int AutoWordBackspaces
        {
            get
            {
                int num = 0;
                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] == (char)EncodeChar.AutoWordBackspace)
                        num++;
                }
                return num;
            }
        }

        // taken from https://www.dotnetperls.com/levenshtein 
        private int LevenshteinDist(string s, string t)
        {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }

        public int AutoDistances
        {
            get
            {
                int num = 0;

                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] == (char)EncodeChar.AutoWordBackspace &&
                        _stream[i + 1] != (char)EncodeChar.AutoInsert)
                    {
                        String deletedWord = "";
                        String insertedWord = "";

                        int j = i - 1;
                        // scan to the left to find the word we deleted
                        do
                        {
                            deletedWord = _stream[j] + deletedWord;
                            j--;
                        } while (j >= 0 && !Char.IsWhiteSpace(_stream[j]));

                        j = i + 1;
                        // scan to the right to find the word we replaced the deleted word with
                        do
                        {
                            insertedWord = insertedWord + _stream[j];
                            j++;
                        } while (_stream[j] != (char)EncodeChar.AutoInsert);

                        num += LevenshteinDist(deletedWord.Trim(), insertedWord.Trim());
                    }

                }
                return num;
            }
        }

        public int ManualDistances
        {
            get
            {
                int num = 0;

                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] == (char)EncodeChar.ManualWordBackspace)
                    {
                        String deletedWord = "";
                        String insertedWord = "";

                        int j = i - 1;
                        // scan to the left to find the word we deleted
                        do
                        {
                            deletedWord = _stream[j] + deletedWord;
                            j--;
                        } while (j >= 0 && !Char.IsWhiteSpace(_stream[j]));

                        j = i + 1;
                        // scan to the right to find the word we replaced the deleted word with
                        do
                        {
                            insertedWord = insertedWord + _stream[j];
                            j++;
                        } while (_stream[j] != (char)EncodeChar.ManualInsert);

                        num += LevenshteinDist(deletedWord, insertedWord);
                    }

                }
                return num;
            }
        }

        // ticha: counts the total number of characters that had to be 
        // replaced by autocorrect (includes character 10 and 12)
        public int AutoWordDeleted
        {
            get
            {
                int num = 0;

                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] == (char)EncodeChar.AutoWordBackspace)
                    {

                        int j = i-1;
                        do
                        {
                            if (_stream[j] != (char)EncodeChar.Nonrecognition)
                                num++;
                            j--;
                        } while (j >= 0 && !Char.IsWhiteSpace(_stream[j]));

                    } 

                }
                return num;
            }
        }

        // ticha: counts the total number of characters that had to be 
        // replaced by manual correct
        public int ManualWordDeleted
        {
            get
            {
                int num = 0;

                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] == (char)EncodeChar.ManualWordBackspace)
                    {

                        int j = i-1;
                        do
                        {
							if (_stream[j] != (char)EncodeChar.Nonrecognition)
								num++;
                            j--;
                        } while (j >= 0 && !Char.IsWhiteSpace(_stream[j]));

                    }

                }
                return num;
            }
        }

        // number of characters between 9&11 and 10&12 (inclusive)
        public int Replacements
        {
            get
            {
                int num = 0;
                int startidx = -1;
                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] == (char)EncodeChar.ManualWordBackspace ||
                        _stream[i] == (char)EncodeChar.AutoWordBackspace)
                    {
                        startidx = i;
                        num++;
                    }

                    else if (_stream[i] == (char)EncodeChar.ManualInsert ||
                             _stream[i] == (char)EncodeChar.AutoInsert)
                    {
                        Debug.Assert(startidx > -1);

                        // append length of the word between 9&11 or 10&12
                        num += i - startidx;
                        startidx = -1;
                    }

                }
                return num;
            }
        }

        // not to be confused with "spaces"!
        public int Spacers
        {
            get
            {
                int num = 0;
                for (int i = 0; i < _stream.Length; i++)
                {
                    if (_stream[i] == (char)EncodeChar.Spacer)
                        num++;
                }
                return num;
            }
        }

        public int Letters
        {
            get
            {
                int num = 0;
                for (int i = 0; i < _stream.Length; i++)
                {
                    if (Char.IsLetter(_stream[i]))
                        num++;
                }
                return num;
            }
        }

        #endregion

        #region String Handling

        public void StreamAlign(ref string P, ref string T)
        {
            // copy the flags into an arraylist since bitarrays don't support insertion
            ArrayList flagscopy = new ArrayList(_flags.Count);
            for (int i = 0; i < _flags.Count; i++)
                flagscopy.Add(_flags[i]);

            string spacer = ((char)EncodeChar.Spacer).ToString();

            // make a forward pass over IS, inserting spacers in T and P where flags 
            // are absent, and in IS where there is a omission in T
            for (int i = 0; i < Math.Max(flagscopy.Count, T.Length); i++)
            {
                if (i < T.Length && T[i] == (char)EncodeChar.InsertionOmission)
                {
                    _stream = _stream.Insert(i, spacer);    // insert an empty slot in IS
                    flagscopy.Insert(i, false);             // keep flags paired
                    _positions.Insert(i, 0);                // keep position values paired
                }
                else if (i < flagscopy.Count && !(bool)flagscopy[i])
                {
                    T = T.Insert(i, spacer);    // insert an empty slot in T
                    P = P.Insert(i, spacer);    // insert an empty slot in P
                }
            }

            // copy from the arraylist back into the flags bitarray
            _flags.Length = flagscopy.Count; // possibly extends the length of flags
            for (int i = 0; i < flagscopy.Count; i++)
                _flags[i] = (bool)flagscopy[i];
        }

        public char NextChar(int start, int count)
        {
            int index = start;
            while (index < _stream.Length && _stream[index] == (char)EncodeChar.Spacer)
                index++;
            while (count > 0)
            {
                if (++index >= _stream.Length)
                    break;
                else if (_stream[index] != (char)EncodeChar.Spacer)
                    count--;
            }
            return (index < _stream.Length ? _stream[index] : '\0');
        }

        public char PrevChar(int start, int count)
        {
            int index = start;
            while (index >= 0 && _stream[index] == (char)EncodeChar.Spacer)
                index--;
            while (count > 0)
            {
                if (--index < 0)
                    break;
                else if (_stream[index] != (char)EncodeChar.Spacer)
                    count--;
            }
            return (index >= 0 ? _stream[index] : '\0');
        }

        #endregion

        #region Flagging & Position

        public string Transcription
        {
            get
            {
                //! this is where the transcribed word is stored
                string ans = String.Empty;
                for (int i = 0; i < _flags.Length; i++)
                {
                    if (_flags[i])
                        ans += _stream[i];
                }
                return ans;
            }
        }

        private void FlagStream()
        {
            // count tells you the number of characters you need to skip
            // as a result of backspaces/wordbackspaces
            int count = 0;
            for (int i = _stream.Length - 1; i >= 0; i--)
            {
                if (_stream[i] == (char)EncodeChar.Backspace)
                {
                    count++;
                }

                //! handles word backspace
                else if (_stream[i] == (char)EncodeChar.ManualWordBackspace ||
                         _stream[i] == (char)EncodeChar.AutoWordBackspace)
                {

                    int j = i - 1;
                    // additional steps that need to be taken as a result of backspace
                    int moresteps = 0;
                    bool foundalpha = false; // checks whether we've seen a normal character

                    if (_stream[i + 1] == (char)EncodeChar.AutoInsert)
                    {
                        count++;
                    }

                    else
                    {
                        while (j >= 0)
                        {
                            // regular backspaces require us to take more steps
                            if (_stream[j] == (char)EncodeChar.Backspace)
                            {
                                moresteps++;
                            }

                            else if (moresteps > 0 &&
                                     ((97 <= _stream[j] && _stream[j] <= 122) || _stream[j] == 32))
                            {
                                moresteps--;
                            }

                            else
                            {
                                if (97 <= _stream[j] && _stream[j] <= 122 && !foundalpha)
                                {
                                    foundalpha = true;
                                }
                                else if (_stream[j] == 32 && foundalpha)
                                {
                                    break; // exits at the second space it finds
                                }

                                if ((97 <= _stream[j] && _stream[j] <= 122) || _stream[j] == 32)
                                    count++;
                            }

                            j--;
                        }
                    }

                    //System.Console.Write(count);
                }

                else if (_stream[i] != (char)EncodeChar.Nonrecognition &&
                         _stream[i] != (char)EncodeChar.ManualInsert &&
                         _stream[i] != (char)EncodeChar.AutoInsert)
                {
                    _flags[i] = (count == 0);
                    if (count > 0)
                        count--;
                }
            }
        }

        public string FlagStr
        {
            get
            {
                string ans = String.Empty;
                for (int i = 0; i < _flags.Count; i++)
                    ans += _flags[i] ? "1 " : "0 ";
                ans.TrimEnd();
                return ans;
            }
        }

        public bool IsFlagged(int index)
        {
            if (index < _flags.Length)
                return _flags[index];

            return false;
        }

        private void AssignPositions()
        {
            int pos = 0;
            for (int i = 0; i < _stream.Length; i++)
            {
                if (_flags[i])
                {
                    _positions[i] = 0;
                    pos = 0;
                }
                else
                {
                    if (_stream[i] == (char)EncodeChar.Backspace)
                    {
                        if (pos > 0)
                            pos--; // decrement, then assign
                        _positions[i] = pos;
                    }

                    // ticha: new function for Manual/AutoWordBackspace
                    else if (_stream[i] == (char)EncodeChar.ManualWordBackspace ||
                             _stream[i] == (char)EncodeChar.AutoWordBackspace)
                    {
                        for (int j = i; !Char.IsWhiteSpace(_stream[j]); j--)
                        {
                            if (pos > 0)
                                pos--; // decrement, then assign
                            _positions[j] = pos;
                        }

                    }
                    else if (_stream[i] == (char)EncodeChar.Nonrecognition || _stream[i] == (char)EncodeChar.Spacer ||
                             _stream[i] == (char)EncodeChar.AutoInsert || _stream[i] == (char)EncodeChar.ManualInsert)
                    {
                        _positions[i] = pos; // just assign
                    }
                    else
                    {
                        _positions[i] = pos;
                        pos++; // assign, then increment
                    }
                }
            }
        }

        public int GetPosition(int index)
        {
            if (0 <= index && index < _positions.Count)
                return ((int)_positions[index]);

            return -1;
        }

        public string PositionStr
        {
            get
            {
                string ans = String.Empty;
                for (int i = 0; i < _positions.Count; i++)
                {
                    int v = (int)_positions[i];
                    ans += v;
                    if (v < 10)
                        ans += " ";
                }
                ans.TrimEnd();
                return ans;
            }
        }

        #endregion
    }
}
