using System;
using System.Text;
using System.Reflection;
using System.IO;

namespace StreamAnalyzer
{
    public class MainObj
    {
        [STAThread]
        static void Main(string[] args)
        {
            Version v = Assembly.GetExecutingAssembly().GetName().Version;
            Console.WriteLine("Character-Level Error Analyzer {0}.{1}.{2} (C) 2005 Carnegie Mellon University", v.Major, v.Minor, v.Build);
            Console.WriteLine("Software written in C# .NET by Jacob O. Wobbrock <jrock@cs.cmu.edu>");
            Console.WriteLine("Algorithms by R.W. Soukoreff, I.S. MacKenzie, J.O. Wobbrock and B.A. Myers");
            Console.WriteLine("Options: -d allows direct entry of P and IS.");

            Analyzer analyzer = new Analyzer();

            string[] log_files = Directory.GetFiles(@"log_files/", "*.xml");

            Console.WriteLine("Analyzing files...");
            for (int i = 0; i < log_files.Length; i++)
            {  
				string infile = log_files[i];
                //Console.Write("\nReading log file: " + infile + "\n");
				string defout = infile.Substring(0, infile.LastIndexOf('.')) + ".txt";

				string outfile = infile + "-analysis.txt";
				if (outfile.Length == 0)
					outfile = defout;

                //Console.WriteLine("Analyzing...");
                bool success = analyzer.Analyze(infile, outfile); 
				if (!success)
                    Console.Write(" : " + infile + "\n");
            }
			Console.WriteLine("Analysis complete.");

		}
    }
}
