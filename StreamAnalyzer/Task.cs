using System;

namespace StreamAnalyzer
{
	public class Task
	{
		public static readonly Task Empty = new Task();

		public int			Num;		// task number
		public bool			IsTest;		// is testing?
		public string		P;			// presented string
		public string		T;			// transcribed string
		public InputStream	IS;			// input stream
		public double		Init;		// when we have initial activity (often first pen down)
		public double		Start;		// start time - when first character is entered
		public double		End;		// end time - when last character is entered
		public double		Intra;		// total intra-character time for task
		public double		Inter;		// total inter-character time for task

		// default constructor
		public Task()
		{
			this.Num	= 0;
			this.IsTest	= false;
			this.P		= String.Empty;
			this.T		= String.Empty;
			this.IS		= null;
			this.Init	= 0D;
			this.Start	= Double.MaxValue;
			this.End	= Double.MinValue;
			this.Intra	= 0D;
			this.Inter	= 0D;
		}

		// copy constructor
		public Task(Task t)
		{
			this.Num	= t.Num;
			this.IsTest = t.IsTest;
			this.P		= t.P;
			this.T		= t.T;
			this.IS		= new InputStream(t.IS);
			this.Init	= t.Init;
			this.Start	= t.Start;
			this.End	= t.End;
			this.Intra	= t.Intra;
			this.Inter	= t.Inter;
		}

		public float Time
		{
			get
			{
				return (float) (End - Start);
			}
		}
	}
}
