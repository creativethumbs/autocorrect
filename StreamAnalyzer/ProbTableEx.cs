using System;
using System.IO;
using System.Diagnostics;

namespace StreamAnalyzer
{
	public class ProbTableEx : ProbTable
	{
		protected readonly int		_nonrecIndex;
		protected readonly int		_miscIndex;
		protected new readonly int	_insIndex;

		// constructor
		public ProbTableEx(char[] charset) : base(charset)
		{
			// use the parent class's charset but extend it by two:
			// one for nonrecognitions, the other for miscellaneous
			TableEntry[] copy = new TableEntry[charset.Length + 3];
			_entries.CopyTo(copy, 0); // parents entries get copied to our 'copy'
			
			_nonrecIndex = charset.Length;
			copy[_nonrecIndex] = new TableEntry((char) EncodeChar.Nonrecognition);
			
			_miscIndex = charset.Length + 1;
			copy[_miscIndex] = new TableEntry((char) EncodeChar.Miscellaneous);

			_insIndex = charset.Length + 2;
			copy[_insIndex] = new TableEntry((char) EncodeChar.InsertionOmission);

			// redirect entries to point to the clone
			_entries = copy;
		}

		protected override int CharToIndex(char ch)
		{
			for (int i = 0; i < _entries.Length; i++)
			{
				if (i != _miscIndex && _entries[i].Char == ch)
					return i;
			}
			return _miscIndex;
		}

		public float TotalPresented
		{
			get
			{
				float total = 0f;
				for (int i = 0; i < _nonrecIndex; i++)
					total += _entries[i].CountP;
				return total;
			}
		}

		public float TotalTranscribed
		{
			get
			{
				float total = 0f;
				for (int i = 0; i < _nonrecIndex; i++)
					total += _entries[i].CountT;
				return total;
			}
		}

		public override void Write(TextWriter writer)
		{
			// do nothing
		}

		// overload
		public void Write(TextWriter writer, ConfusionMatrix matrix)
		{
			writer.WriteLine();
			writer.Write("-------- Wobbrock & Myers Character Count ---------");	
			writer.Write("\t--- Error Rates	---");
			writer.Write("\t----- Substitutions :: Intended -----");
			writer.Write("\t--- Omissions :: Presented --");
			writer.Write("\t--- Insertions :: Entered ---");
			writer.WriteLine();

			writer.Write("Char\tPresent\tTranscr\tEntered\tIntnded\tCorrect\tNRs");
			writer.Write("\tUnc\tCor\tTotal");
			writer.Write("\tCount\tUnc\tCor\tNR\tTotal");	// substitutions
			writer.Write("\tCount\tUnc\tCor\tTotal");		// omissions
			writer.Write("\tCount\tUnc\tCor\tTotal");		// insertions
			writer.WriteLine();

			// variables to hold whole-table values
			float nPres = 0f, nTrans = 0f, nEntered = 0f, nIntended = 0f, nCorrect = 0f, nNRs = 0f;
			float nUncNoerr = 0f, nCorNoerr = 0f;						// no-errors
			float nSub = 0f, nUncSub = 0f, nCorSub = 0f, nNRSub = 0f;	// substitutions
			float nOmi = 0f, nUncOmi = 0f, nCorOmi = 0f;				// omissions
			float nIns = 0f, nUncIns = 0f, nCorIns = 0f;				// insertions

			for (int i = 0; i < _insIndex; i++)
			{
				// store the character that this entry refers to
				char ch = _entries[i].Char;
				
				//
				// Category Counts
				//

				// the number of times this character was presented
				float pres = _entries[i].CountP;
				nPres += pres;

				// the number of times this character was transcribed
				float trans = _entries[i].CountT;
				nTrans += trans;

				/**
				 * The number of times this character was entered is the number of times it was 
				 * produced, which is the sum of its matrix row, plus the number of times it was
				 * inserted. Note that no letters will have any nonrec insertions, and the nonrec
				 * character will only have nonrec insertions and no other insertion types.
				 */
				float entered = matrix.GetNumEntered(ch) + _entries[i].UncorrectedInsertions + _entries[i].CorrectedInsertions; //+ _entries[i].NonrecInsertions;
				nEntered += entered;

				/**
				 * The number of times a character was intended (attempted) is given by the sum of the 
				 * matrix column. Note that 'intended' could be less than 'entered', if it was entered
				 * accidentally (and therefore, unintended).
				 */
				float intended = matrix.GetNumIntended(ch);
				nIntended += intended;
				
				/**
				 * The number of times this letter was entered correctly is given by matrix[x,x] for any 
				 * character x. This should be the same as _entries[i].NoErrors.
				 */
				float correct = matrix.GetNumCorrect(ch);
				Debug.Assert(Math.Abs(correct - _entries[i].NoErrors) < 1f);
				nCorrect += correct;

				/**
				 * The number of non-recognitions for this character is the number of times it was intended 
				 * but a non-recognition occured. We can get this from either the confusion matrix or the
				 * table entry directly.
				 */
				float nonrecs = matrix.GetNumNonRecs(ch);
				Debug.Assert(Math.Abs(nonrecs - _entries[i].NonrecSubstitutions) < 1f);
				nNRs += nonrecs;

				writer.Write("{0}\t{1:f3}\t{2:f3}\t{3:f3}\t{4:f3}\t{5:f3}\t{6:f3}\t", ch, pres, trans, entered, intended, correct, nonrecs);
				
				//
				// Letterwise Error Rates
				//

				/**
				 * "What percentage of the uncorrected x's were erroneous?" = Uncorrected Error Rate for a letter
				 * "What percentage of the corrected x's were erroneous?" = Corrected Error Rate for a letter
				 * "What percentage of the x's entered were erroneous?" = Total Error Rate for a letter
				 */

				float uncErr = (trans > 0f) ? (1f - _entries[i].UncorrectedNoErrors / trans) : 0f;
				float corErr = (entered - trans > 0f) ? (1f - _entries[i].CorrectedNoErrors / (entered - trans)) : 0f;
				float totErr = (entered > 0f) ? (1f - (_entries[i].UncorrectedNoErrors + _entries[i].CorrectedNoErrors) / entered) : 0f;

				nUncNoerr += _entries[i].UncorrectedNoErrors;
				nCorNoerr += _entries[i].CorrectedNoErrors;

				writer.Write("{0:f3}\t{1:f3}\t{2:f3}\t", uncErr, corErr, totErr);

				//
				// Substitutions vs. Intended
				//

				/**
				 * The ratio of a letter's substitutions to the number of times it was intended
				 * tells us the probability of getting a substitution given an intention to enter
				 * a letter.
				 */
				float subCount = intended - correct;
				Debug.Assert(Math.Abs(subCount - (_entries[i].UncorrectedSubstitutions + _entries[i].CorrectedSubstitutions + _entries[i].NonrecSubstitutions)) < 1f);
				nSub += subCount;

				float subUnc = (intended > 0f ? _entries[i].UncorrectedSubstitutions / intended : 0f);
				nUncSub += _entries[i].UncorrectedSubstitutions;

				float subCor = (intended > 0f ? _entries[i].CorrectedSubstitutions / intended : 0f);
				nCorSub += _entries[i].CorrectedSubstitutions;

				float subNR = (intended > 0f ? _entries[i].NonrecSubstitutions / intended : 0f);
				nNRSub += _entries[i].NonrecSubstitutions;
				
				Debug.Assert(Math.Abs(_entries[i].NonrecSubstitutions - matrix.GetNumNonRecs(ch)) < 1f);
				Debug.Assert(Math.Abs(subNR - matrix.GetProbNonRec(ch)) < 1f);

				writer.Write("{0:f3}\t{1:f3}\t{2:f3}\t{3:f3}\t{4:f3}\t", subCount, subUnc, subCor, subNR, subUnc + subCor + subNR);

				//
				// Omissions vs. Presented
				//
				
				/**
				 * By taking the ratio of the number of times a letter is omitted to the number
				 * of times it was presented, we can get an idea of which letters are getting
				 * omitted more than others. Note that for every 1 presented letter we can
				 * only have at most 1 uncorrected omission, but we can have an arbitrary number of
				 * corrected omissions.
				 */
				float omiCount = _entries[i].UncorrectedOmissions + _entries[i].CorrectedOmissions;
				nOmi += omiCount;
				
				float omiUnc = (pres > 0f ? _entries[i].UncorrectedOmissions / pres : 0f);
				nUncOmi += _entries[i].UncorrectedOmissions;

				float omiCor = (pres > 0f ? _entries[i].CorrectedOmissions / pres : 0f);
				nCorOmi += _entries[i].CorrectedOmissions;

				writer.Write("{0:f3}\t{1:f3}\t{2:f3}\t{3:f3}\t", omiCount, omiUnc, omiCor, omiUnc + omiCor);

				//
				// Insertions vs. Entered
				//

				/**
				 * We can get an idea of which letters are inserted by taking the ratio of the number of
				 * times they are inserted to the total number of times they are entered. This answers
				 * the question, "given an entry of this letter, what is the probability that it was
				 * inserted?"
				 */
				float insCount = _entries[i].UncorrectedInsertions + _entries[i].CorrectedInsertions; //+ _entries[i].NonrecInsertions;
				nIns += insCount;

				float insUnc = (entered > 0f ? _entries[i].UncorrectedInsertions / entered : 0f);
				nUncIns += _entries[i].UncorrectedInsertions;

				float insCor = (entered > 0f ? _entries[i].CorrectedInsertions / entered : 0f);
				nCorIns += _entries[i].CorrectedInsertions;
				
				//float insNR = (entered > 0f ? _entries[i].NonrecInsertions / entered : 0f);
				//nNRIns += _entries[i].NonrecInsertions;

				writer.WriteLine("{0:f3}\t{1:f3}\t{2:f3}\t{3:f3}", insCount, insUnc, insCor, /*insNR,*/ insUnc + insCor /*+ insNR*/);
			}

			//
			// Whole-Table Results
			//
			
			// error rates
			float nTotErr = (nEntered > 0f) ? (1f - (nUncNoerr + nCorNoerr) / nEntered) : 0f;
			nUncNoerr = (nTrans > 0f) ? (1f - nUncNoerr / nTrans) : 0f;
			nCorNoerr = (nEntered - nTrans > 0f) ? (1f - nCorNoerr / (nEntered - nTrans)) : 0f;

			// substitutions
			Debug.Assert(Math.Abs(nSub - matrix.TotalIncorrect) < 1f);
			nUncSub /= nIntended;
			nCorSub /= nIntended;
			nNRSub /= nIntended;
			float totSub = nSub / nIntended;

			// omissions
			Debug.Assert(Math.Abs(nUncOmi - _entries[_insIndex].CountT) < 1f);
			nUncOmi /= nPres;
			nCorOmi /= nPres;
			float totOmi = nOmi / nPres;

			// insertions
			Debug.Assert(Math.Abs(nUncIns - _entries[_insIndex].CountP) < 1f);
			nUncIns /= nEntered;
			nCorIns /= nEntered;
			//nNRIns /= nEntered;
			float totIns = nIns / nEntered;

			writer.Write("Total\t{0:f3}\t{1:f3}\t{2:f3}\t{3:f3}\t{4:f3}\t{5:f3}\t", nPres, nTrans, nEntered, nIntended, nCorrect, nNRs);
			writer.Write("{0:f3}\t{1:f3}\t{2:f3}\t", nUncNoerr, nCorNoerr, nTotErr);
		
			writer.Write("{0:f3}\t{1:f3}\t{2:f3}\t{3:f3}\t{4:f3}\t", nSub, nUncSub, nCorSub, nNRSub, totSub);
			writer.Write("{0:f3}\t{1:f3}\t{2:f3}\t{3:f3}\t", nOmi, nUncOmi, nCorOmi, totOmi);
			writer.WriteLine("{0:f3}\t{1:f3}\t{2:f3}\t{3:f3}", nIns, nUncIns, nCorIns, /*nNRIns,*/ totIns);
		}
	}
}
