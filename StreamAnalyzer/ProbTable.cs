using System;
using System.IO;

namespace StreamAnalyzer
{
	public class ProbTable
	{
		// member variables
		protected readonly int	_insIndex;
		protected TableEntry[]	_entries;

		// constructor
		public ProbTable(char[] charset)
		{
			// have an entry for each character in the charset, and one extra for the insertion character
			_entries = new TableEntry[charset.Length + 1];
			for (int i = 0; i < charset.Length; i++)
				_entries[i] = new TableEntry(charset[i]); // make a table entry for each charset char

			// add the insertion char to our list of characters
			_insIndex = charset.Length;
			_entries[_insIndex] = new TableEntry((char) EncodeChar.InsertionOmission);
		}

		public void Clear()
		{
			// clear each table entry item
			for (int i = 0; i < _entries.Length; i++)
				_entries[i].Clear();
		}

		/// <summary>
		/// Counts and stores the number of characters in either P or T. 
		/// This includes uncorrected insertions (in P) and uncorrected 
		/// omissions (in T). These counts are weighted by the inverse
		/// of the number of alignments in which they occur. See [1]
		/// for more details.
		/// </summary>
		/// <param name="PorT"></param>
		/// <param name="numAlign"></param>
		/// <param name="isP"></param>
		public void Count(string PorT, bool isP, float numAlign)
		{
			for (int i = 0; i < PorT.Length; i++)
			{
				char ch = PorT[i];
				int k = CharToIndex(ch);
				
				if (k != -1)
				{
					if (isP)
						_entries[k].CountP += (1f / numAlign);
					else
						_entries[k].CountT += (1f / numAlign);
				}
			}
		}

		/// <summary>
		/// Given a character, return the row index of the table entry for that character. 
		/// </summary>
		/// <param name="ch">The character whose index we wish to find.</param>
		/// <returns>If the character is found, the index in the table; otherwise -1.</returns>
		protected virtual int CharToIndex(char ch)
		{
			for (int i = 0; i < _entries.Length; i++)
			{
				if (_entries[i].Char == ch)
					return i;
			}
			return -1;
		}

		#region Row Statistics

		// gets the number of times the given character was presented in P
		public float GetCount(char ch)
		{
			int k = CharToIndex(ch);
			return _entries[k].CountP;
		}

		/// <summary>
		/// Adds an error of the given type to the given letter, weighted by the inverse of 
		/// the given number of alignments. For the mathematical reasoning behind this, see
		/// [1].
		/// </summary>
		/// <param name="type">The enumerated error type.</param>
		/// <param name="ch">The charater for which this error occured.</param>
		/// <param name="numAlign">The number of alignments in the alignment set in which
		/// this error occured.</param>
		public void AddEntry(ErrorType type, char ch, float numAlign)
		{
			int k = CharToIndex(ch);
			if (k != -1)
				_entries[k].AddToError(type, 1f / numAlign);
		}

		// gets the probability of a given error type for the given character
		public float GetProbability(ErrorType type, char ch)
		{
			int k = CharToIndex(ch);
			return _entries[k].GetErrorProbability(type);
		}

		#endregion

		#region Table Statistics

		/// <summary>
		/// The total count of characters includes its insertions. This is because the
		/// P's are counted after alignments with T's. This property gets the total count
		/// by summing up over the table entries for each character.
		/// </summary>
		public float TotalCount
		{
			get
			{
				float total = 0f;
				for (int i = 0; i < _entries.Length; i++)
					total += _entries[i].CountP;
				return total;
			}
		}

		public float TotalInsertions
		{
			get
			{
				// no need to sum over insertions column since only the
				// insertion character can have a nonzero insertion count
				return _entries[_insIndex].CountP;
			}
		}

		// Weighted by count for each letter. That is, equivalent to 
		// _entries[i].GetErrorProbability(ErrorType.UncorrectedSubstitutions) * _entries[i].CountP
		public float TotalSubstitutions
		{
			get
			{
				float total = 0f;
				for (int i = 0; i < _entries.Length; i++)
					total += _entries[i].UncorrectedSubstitutions;
				return total;
			}
		}

		// Weighted by count for each letter. That is, equivalent to 
		// _entries[i].GetErrorProbability(ErrorType.UncorrectedOmissions) * _entries[i].CountP
		public float TotalOmissions
		{
			get
			{
				float total = 0f;
				for (int i = 0; i < _entries.Length; i++)
					total += _entries[i].UncorrectedOmissions;
				return total;
			}
		}

		public float AvgInsertions
		{
			get
			{
				float count = TotalCount;
				return (count > 0f ? TotalInsertions / count : 0f);
			}
		}

		public float AvgSubstitutions
		{
			get
			{
				float count = TotalCount;
				return (count > 0f ? TotalSubstitutions / count : 0f);
			}
		}

		public float AvgOmissions
		{
			get
			{
				float count = TotalCount;
				return (count > 0f ? TotalOmissions / count : 0f);
			}
		}

		#endregion

		/// <summary>
		/// This method writes a character-level probability table that, for each letter in
		/// the character set, tells us the number of times that character was presented, and
		/// the probabilities that the character was subsituted or omitted. We also get the odds 
		/// of an insertion error occuring. This table is composed without any input stream 
		/// information. This table is just like Tables 1 and 2 from [1].
		/// </summary>
		public virtual void Write(TextWriter writer)
		{
			// write the table row by row, one row per character
			writer.WriteLine();
			writer.WriteLine("-- MacKenzie & Soukoreff NordiCHI '02 Table --");
			writer.WriteLine("Char\tCount\tIns\tSub\tDel\tTotal");
			writer.WriteLine("\t\t------------------------------");
			
			for (int i = 0; i < _entries.Length; i++)
			{
				char ch = (_entries[i].Char == (char) EncodeChar.InsertionOmission ? (char) DisplayChar.InsertionOmission : _entries[i].Char);
				float cnt = _entries[i].CountP;
				float ins = (_entries[i].Char == (char) EncodeChar.InsertionOmission ? 1f : 0f);
				float sub = _entries[i].GetErrorProbability(ErrorType.UncorrectedSubstitution);
				float del = _entries[i].GetErrorProbability(ErrorType.UncorrectedOmission);
				writer.WriteLine("{0}\t{1:f3}\t{2}\t{3:f3}\t{4:f3}\t{5:f3}", ch, cnt, ins, sub, del, ins + sub + del);
			}
			// compute and write the Total row
			float tcnt = TotalCount;
			float tins = TotalInsertions;
			float tsub = TotalSubstitutions;
			float tdel = TotalOmissions;
			writer.WriteLine("Total\t{0:f3}\t{1:f3}\t{2:f3}\t{3:f3}\t{4:f3}", tcnt, tins, tsub, tdel, tins + tsub + tdel);

			// compute and write the Average row
			float ains = AvgInsertions;
			float asub = AvgSubstitutions;
			float adel = AvgOmissions;
			writer.WriteLine("Average\t\t{0:f3}\t{1:f3}\t{2:f3}\t{3:f3}", ains, asub, adel, ains + asub + adel);
		}
	}
}
