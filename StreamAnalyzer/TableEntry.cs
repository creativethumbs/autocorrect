using System;

namespace StreamAnalyzer
{
	public class TableEntry
	{
		public char			Char;		// the char for this table entry
		public float		CountP;		// number of times presented
		public float		CountT;		// number of times transcribed
		
		protected float[]	_errors;	// counts for each of the 10 error types

		public TableEntry(char ch)
		{
			this.Char = ch;
			this.CountP = 0f;
			this.CountT = 0f;
			
			_errors = new float[(int) ErrorType.NumErrorTypes];
		}

		public override string ToString()
		{
			return Char.ToString();
		}

		// does not clear the character itself
		public void Clear()
		{
			this.CountP = 0f;	// number of times this letter was presented
			this.CountT = 0f;	// number of times this letter was transcribed
			Array.Clear(_errors, 0, _errors.Length);
		}

		public void AddToError(ErrorType type, float fValue)
		{
			_errors[(int) type] += fValue;
		}

		public void SetErrors(ErrorType type, float fValue)
		{
			_errors[(int) type] = fValue;
		}

		public float GetErrors(ErrorType type)
		{
			return _errors[(int) type];
		}

		public float GetErrorProbability(ErrorType type)
		{
			return (CountP > 0f ? _errors[(int) type] / CountP : 0f);
		}

		public float NoErrors
		{
			get { return (UncorrectedNoErrors + CorrectedNoErrors); }
		}

		public float UncorrectedNoErrors
		{
			get { return _errors[(int) ErrorType.UncorrectedNoError];  }
			set { _errors[(int) ErrorType.UncorrectedNoError] = value; }
		}

		public float CorrectedNoErrors
		{
			get { return _errors[(int) ErrorType.CorrectedNoError];  }
			set { _errors[(int) ErrorType.CorrectedNoError] = value; }
		}

		public float UncorrectedOmissions
		{
			get { return _errors[(int) ErrorType.UncorrectedOmission];	}
			set { _errors[(int) ErrorType.UncorrectedOmission] = value; }
		}

		public float UncorrectedInsertions
		{
			get { return _errors[(int) ErrorType.UncorrectedInsertion];  }
			set { _errors[(int) ErrorType.UncorrectedInsertion] = value; }
		}

		public float UncorrectedSubstitutions
		{
			get { return _errors[(int) ErrorType.UncorrectedSubstitution];  }
			set { _errors[(int) ErrorType.UncorrectedSubstitution] = value; }
		}

		public float CorrectedOmissions
		{
			get { return _errors[(int) ErrorType.CorrectedOmission];  }
			set { _errors[(int) ErrorType.CorrectedOmission] = value; }
		}

		public float CorrectedInsertions
		{
			get { return _errors[(int) ErrorType.CorrectedInsertion];  }
			set { _errors[(int) ErrorType.CorrectedInsertion] = value; }
		}

		public float CorrectedSubstitutions
		{
			get { return _errors[(int) ErrorType.CorrectedSubstitution];  }
			set { _errors[(int) ErrorType.CorrectedSubstitution] = value; }
		}
			
		public float NonrecInsertions
		{
			get { return _errors[(int) ErrorType.NonrecInsertion];  }
			set { _errors[(int) ErrorType.NonrecInsertion] = value; }
		}
		
		public float NonrecSubstitutions
		{
			get { return _errors[(int) ErrorType.NonrecSubstitution];  }
			set { _errors[(int) ErrorType.NonrecSubstitution] = value; }
		}
	}
}
